--- /dev/null	2003-01-30 15:54:37.000000000 +0530
+++ connectivity/source/drivers/evoab1.5/NStatement.hxx	2004-03-18 14:52:50.000000000 +0530
@@ -0,0 +1,224 @@
+/*************************************************************************
+ *
+ *  $RCSfile$
+ *
+ *  $Revision$
+ *
+ *  last change: $Author$ $Date$
+ *
+ *  The Contents of this file are made available subject to the terms of
+ *  the BSD license.
+ *  
+ *  Copyright (c) 2003 by Sun Microsystems, Inc.
+ *  All rights reserved.
+ *
+ *  Redistribution and use in source and binary forms, with or without
+ *  modification, are permitted provided that the following conditions
+ *  are met:
+ *  1. Redistributions of source code must retain the above copyright
+ *     notice, this list of conditions and the following disclaimer.
+ *  2. Redistributions in binary form must reproduce the above copyright
+ *     notice, this list of conditions and the following disclaimer in the
+ *     documentation and/or other materials provided with the distribution.
+ *  3. Neither the name of Sun Microsystems, Inc. nor the names of its
+ *     contributors may be used to endorse or promote products derived
+ *     from this software without specific prior written permission.
+ *
+ *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
+ *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
+ *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
+ *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
+ *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
+ *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
+ *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
+ *  OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
+ *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
+ *  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
+ *  USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+ *     
+ *************************************************************************/
+
+#ifndef _CONNECTIVITY_EVOAB_STATEMENT_HXX_
+#define _CONNECTIVITY_EVOAB_STATEMENT_HXX_
+
+#ifndef _COM_SUN_STAR_SDBC_XSTATEMENT_HPP_
+#include <com/sun/star/sdbc/XStatement.hpp>
+#endif
+#ifndef _COM_SUN_STAR_SDBC_XWARNINGSSUPPLIER_HPP_
+#include <com/sun/star/sdbc/XWarningsSupplier.hpp>
+#endif
+#ifndef _COM_SUN_STAR_SDBC_XMULTIPLERESULTS_HPP_
+#include <com/sun/star/sdbc/XMultipleResults.hpp>
+#endif
+#ifndef _COM_SUN_STAR_SDBC_XCLOSEABLE_HPP_
+#include <com/sun/star/sdbc/XCloseable.hpp>
+#endif
+#ifndef _COM_SUN_STAR_SDBC_SQLWARNING_HPP_
+#include <com/sun/star/sdbc/SQLWarning.hpp>
+#endif
+#ifndef _COMPHELPER_PROPERTY_ARRAY_HELPER_HXX_
+#include <comphelper/proparrhlp.hxx>
+#endif
+#ifndef _CPPUHELPER_COMPBASE3_HXX_
+#include <cppuhelper/compbase3.hxx>
+#endif
+#ifndef _COMPHELPER_UNO3_HXX_
+#include <comphelper/uno3.hxx>
+#endif
+#ifndef _CONNECTIVITY_COMMONTOOLS_HXX_
+#include "connectivity/CommonTools.hxx"
+#endif
+#ifndef INCLUDED_LIST
+#include <list>
+#define INCLUDED_LIST
+#endif
+#ifndef _COM_SUN_STAR_LANG_XSERVICEINFO_HPP_
+#include <com/sun/star/lang/XServiceInfo.hpp>
+#endif
+#ifndef _COMPHELPER_BROADCASTHELPER_HXX_
+#include <comphelper/broadcasthelper.hxx>
+#endif
+#ifndef _CONNECTIVITY_PARSE_SQLITERATOR_HXX_
+#include "connectivity/sqliterator.hxx"
+#endif
+#ifndef _CONNECTIVITY_PARSE_SQLPARSE_HXX_
+#include "connectivity/sqlparse.hxx"
+#endif
+#ifndef _CONNECTIVITY_FILE_VALUE_HXX_
+#include <connectivity/FValue.hxx>
+#endif
+#ifndef _CONNECTIVITY_OSUBCOMPONENT_HXX_
+#include "OSubComponent.hxx"
+#endif
+#ifndef _COM_SUN_STAR_UTIL_XCANCELLABLE_HPP_
+#include <com/sun/star/util/XCancellable.hpp>
+#endif
+#ifndef _CPPUHELPER_COMPBASE5_HXX_
+#include <cppuhelper/compbase5.hxx>
+#endif
+#ifndef _COM_SUN_STAR_LANG_XSERVICEINFO_HPP_
+#include <com/sun/star/lang/XServiceInfo.hpp>
+#endif
+
+namespace connectivity
+{
+	namespace evoab
+	{
+		class OEvoabResultSet;
+		class OEvoabConnection;
+		typedef ::cppu::WeakComponentImplHelper3<	::com::sun::star::sdbc::XStatement,
+								::com::sun::star::sdbc::XWarningsSupplier,
+								::com::sun::star::sdbc::XCloseable> OStatement_BASE;
+
+		//**************************************************************
+		//************ Class: OStatement_Base
+		// is a base class for the normal statement and for the prepared statement
+		//**************************************************************
+		class OStatement_Base		:	public comphelper::OBaseMutex,
+							public	OStatement_BASE,
+							public	::cppu::OPropertySetHelper,
+							public	::comphelper::OPropertyArrayUsageHelper<OStatement_Base>
+		
+		{
+			::com::sun::star::sdbc::SQLWarning                            m_aLastWarning;
+		protected:
+			::com::sun::star::uno::WeakReference< ::com::sun::star::sdbc::XResultSet>    m_xResultSet;   // The last ResultSet created
+			OEvoabResultSet*                                                                  m_pResultSet;
+			//  for this Statement
+			
+			OEvoabConnection*								m_pConnection;	// The owning Connection object
+		protected:
+			
+			void disposeResultSet();
+			
+			// OPropertyArrayUsageHelper
+			virtual ::cppu::IPropertyArrayHelper* createArrayHelper() const;
+			// OPropertySetHelper
+			virtual ::cppu::IPropertyArrayHelper & SAL_CALL getInfoHelper();
+			virtual sal_Bool SAL_CALL convertFastPropertyValue(
+									   ::com::sun::star::uno::Any & rConvertedValue,
+									   ::com::sun::star::uno::Any & rOldValue,
+									   sal_Int32 nHandle,
+									   const ::com::sun::star::uno::Any& rValue )
+				throw (::com::sun::star::lang::IllegalArgumentException);
+			virtual void SAL_CALL setFastPropertyValue_NoBroadcast(
+									       sal_Int32 nHandle,
+									       const ::com::sun::star::uno::Any& rValue)	throw (::com::sun::star::uno::Exception);
+			virtual void SAL_CALL getFastPropertyValue(
+								   ::com::sun::star::uno::Any& rValue,
+								   sal_Int32 nHandle) const;
+			virtual ~OStatement_Base();
+			
+		protected:
+			/* Driver Internal Methods */
+			OEvoabResultSet*  createResultSet();
+
+			void         reset () throw( ::com::sun::star::sdbc::SQLException);
+			void         clearMyResultSet () throw( ::com::sun::star::sdbc::SQLException);
+
+			
+		public:
+			
+			// other methods
+			OEvoabConnection* getOwnConnection() const { return m_pConnection;}
+			::cppu::OBroadcastHelper& rBHelper;
+			
+			OStatement_Base(OEvoabConnection* _pConnection );
+			using OStatement_BASE::operator ::com::sun::star::uno::Reference< ::com::sun::star::uno::XInterface >;
+			
+			// OComponentHelper
+			virtual void SAL_CALL disposing(void){OStatement_BASE::disposing();}
+			// XInterface
+			virtual void SAL_CALL release() throw();
+			virtual void SAL_CALL acquire() throw();
+			// XInterface
+			virtual ::com::sun::star::uno::Any SAL_CALL queryInterface( const ::com::sun::star::uno::Type & rType ) throw(::com::sun::star::uno::RuntimeException);
+			//XTypeProvider
+			virtual ::com::sun::star::uno::Sequence< ::com::sun::star::uno::Type > SAL_CALL getTypes(  ) throw(::com::sun::star::uno::RuntimeException);
+			
+			// XPropertySet
+			virtual ::com::sun::star::uno::Reference< ::com::sun::star::beans::XPropertySetInfo > SAL_CALL getPropertySetInfo(  ) throw(::com::sun::star::uno::RuntimeException);
+			// XStatement
+			virtual ::com::sun::star::uno::Reference< ::com::sun::star::sdbc::XResultSet > SAL_CALL executeQuery( const ::rtl::OUString& sql ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException) ;
+			virtual sal_Int32 SAL_CALL executeUpdate( const ::rtl::OUString& sql ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException) ;
+			virtual sal_Bool SAL_CALL execute( const ::rtl::OUString& sql ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException) ;
+			virtual ::com::sun::star::uno::Reference< ::com::sun::star::sdbc::XConnection > SAL_CALL getConnection(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException) ;
+			// XWarningsSupplier
+			virtual ::com::sun::star::uno::Any SAL_CALL getWarnings(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual void SAL_CALL clearWarnings(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			// XCloseable
+			virtual void SAL_CALL close(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			
+			};
+		
+		class OStatement_BASE2	: public OStatement_Base,
+					  public ::connectivity::OSubComponent<OStatement_BASE2, OStatement_BASE>
+		
+		{
+			friend class OSubComponent<OStatement_BASE2, OStatement_BASE>;
+		public:
+			OStatement_BASE2(OEvoabConnection* _pConnection ) :  OStatement_Base(_pConnection ),
+									     ::connectivity::OSubComponent<OStatement_BASE2, OStatement_BASE>((::cppu::OWeakObject*)_pConnection, this){}
+			// OComponentHelper
+			virtual void SAL_CALL disposing(void);
+			// XInterface
+			virtual void SAL_CALL release() throw();
+		};
+		
+		class OStatement :	public OStatement_BASE2,
+					public ::com::sun::star::lang::XServiceInfo
+		{
+		protected:
+			virtual ~OStatement(){}
+		public:
+			// ein Konstruktor, der fuer das Returnen des Objektes benoetigt wird:
+			OStatement( OEvoabConnection* _pConnection) : OStatement_BASE2( _pConnection){}
+			DECLARE_SERVICE_INFO();
+			
+			virtual ::com::sun::star::uno::Any SAL_CALL queryInterface( const ::com::sun::star::uno::Type & rType ) throw(::com::sun::star::uno::RuntimeException);
+			virtual void SAL_CALL acquire() throw();
+			virtual void SAL_CALL release() throw();
+		};
+	}
+}
+#endif // CONNECTIVITY_SSTATEMENT_HXX
--- /dev/null	2003-01-30 15:54:37.000000000 +0530
+++ connectivity/source/drivers/evoab1.5/NStatement.cxx	2004-03-18 15:00:05.000000000 +0530
@@ -0,0 +1,417 @@
+/*************************************************************************
+ *
+ *  $RCSfile$
+ *
+ *  $Revision$
+ *
+ *  last change: $Author$ $Date$
+ *
+ *  The Contents of this file are made available subject to the terms of
+ *  the BSD license.
+ *  
+ *  Copyright (c) 2003 by Sun Microsystems, Inc.
+ *  All rights reserved.
+ *
+ *  Redistribution and use in source and binary forms, with or without
+ *  modification, are permitted provided that the following conditions
+ *  are met:
+ *  1. Redistributions of source code must retain the above copyright
+ *     notice, this list of conditions and the following disclaimer.
+ *  2. Redistributions in binary form must reproduce the above copyright
+ *     notice, this list of conditions and the following disclaimer in the
+ *     documentation and/or other materials provided with the distribution.
+ *  3. Neither the name of Sun Microsystems, Inc. nor the names of its
+ *     contributors may be used to endorse or promote products derived
+ *     from this software without specific prior written permission.
+ *
+ *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
+ *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
+ *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
+ *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
+ *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
+ *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
+ *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
+ *  OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
+ *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
+ *  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
+ *  USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+ *     
+ *************************************************************************/
+
+#include <stdio.h>
+
+#ifndef _OSL_DIAGNOSE_H_
+#include <osl/diagnose.h>
+#endif
+
+#ifndef _OSL_THREAD_H_
+#include <osl/thread.h>
+#endif
+
+#ifndef _COM_SUN_STAR_SDBC_RESULTSETCONCURRENCY_HPP_
+#include <com/sun/star/sdbc/ResultSetConcurrency.hpp>
+#endif
+#ifndef _COM_SUN_STAR_SDBC_RESULTSETTYPE_HPP_
+#include <com/sun/star/sdbc/ResultSetType.hpp>
+#endif
+#ifndef _COM_SUN_STAR_SDBC_FETCHDIRECTION_HPP_
+#include <com/sun/star/sdbc/FetchDirection.hpp>
+#endif
+#ifndef _COM_SUN_STAR_LANG_DISPOSEDEXCEPTION_HPP_
+#include <com/sun/star/lang/DisposedException.hpp>
+#endif
+#ifndef _CPPUHELPER_TYPEPROVIDER_HXX_
+#include <cppuhelper/typeprovider.hxx>
+#endif
+#ifndef _CONNECTIVITY_PROPERTYIDS_HXX_
+#include "propertyids.hxx"
+#endif
+#ifndef _CONNECTIVITY_EVOAB_STATEMENT_HXX_
+#include "NStatement.hxx"
+#endif
+#ifndef _CONNECTIVITY_EVOAB_CONNECTION_HXX_
+#include "NConnection.hxx"
+#endif
+#ifndef _CONNECTIVITY_EVOAB_RESULTSET_HXX_
+#include "NResultSet.hxx"
+#endif
+
+using namespace connectivity::evoab;
+//------------------------------------------------------------------------------
+using namespace com::sun::star::uno;
+using namespace com::sun::star::lang;
+using namespace com::sun::star::beans;
+using namespace com::sun::star::sdbc;
+using namespace com::sun::star::sdbcx;
+using namespace com::sun::star::container;
+using namespace com::sun::star::io;
+using namespace com::sun::star::util;
+//------------------------------------------------------------------------------
+OStatement_Base::OStatement_Base(OEvoabConnection* _pConnection) 
+	: OStatement_BASE(m_aMutex),
+	  OPropertySetHelper(OStatement_BASE::rBHelper),
+	  rBHelper(OStatement_BASE::rBHelper),
+	  m_pConnection(_pConnection),
+	  m_xResultSet(NULL),
+	  m_pResultSet(NULL)
+{
+	m_pConnection->acquire();
+}
+// -----------------------------------------------------------------------------
+OStatement_Base::~OStatement_Base()
+{
+}
+//------------------------------------------------------------------------------
+void OStatement_Base::disposeResultSet()
+{
+	// free the cursor if alive
+	Reference< XComponent > xComp(m_xResultSet.get(), UNO_QUERY);
+	if (xComp.is())
+		xComp->dispose();
+	m_xResultSet = Reference< XResultSet>();
+}
+//------------------------------------------------------------------------------
+void OStatement_BASE2::disposing()
+{
+	::osl::MutexGuard aGuard(m_aMutex);
+
+	disposeResultSet();
+
+	if (m_pConnection)
+		m_pConnection->release();
+	m_pConnection = NULL;
+
+	dispose_ChildImpl();
+	OStatement_Base::disposing();
+}
+//-----------------------------------------------------------------------------
+void SAL_CALL OStatement_BASE2::release() throw()
+{
+	relase_ChildImpl();
+}
+//-----------------------------------------------------------------------------
+Any SAL_CALL OStatement_Base::queryInterface( const Type & rType ) throw(RuntimeException)
+{
+	Any aRet = OStatement_BASE::queryInterface(rType);
+	if(!aRet.hasValue())
+		aRet = OPropertySetHelper::queryInterface(rType);
+	return aRet;
+}
+// -------------------------------------------------------------------------
+Sequence< Type > SAL_CALL OStatement_Base::getTypes(  ) throw(RuntimeException)
+{
+	::cppu::OTypeCollection aTypes(	::getCppuType( (const Reference< XMultiPropertySet > *)0 ),
+									::getCppuType( (const Reference< XFastPropertySet > *)0 ),
+									::getCppuType( (const Reference< XPropertySet > *)0 ));
+
+	return ::comphelper::concatSequences(aTypes.getTypes(),OStatement_BASE::getTypes());
+}
+// -------------------------------------------------------------------------
+
+//void SAL_CALL OStatement_Base::cancel(  ) throw(RuntimeException)
+//{
+//::osl::MutexGuard aGuard( m_aMutex );
+//checkDisposed(OStatement_BASE::rBHelper.bDisposed);
+//// cancel the current sql statement
+//}
+
+// -------------------------------------------------------------------------
+void SAL_CALL OStatement_Base::close(  ) throw(SQLException, RuntimeException)
+{
+	{
+		::osl::MutexGuard aGuard( m_aMutex );
+		checkDisposed(OStatement_BASE::rBHelper.bDisposed);
+			
+	}
+	dispose();
+}
+// -------------------------------------------------------------------------
+
+void OStatement_Base::reset() throw (SQLException)
+{
+	::osl::MutexGuard aGuard( m_aMutex );
+	checkDisposed(OStatement_BASE::rBHelper.bDisposed);
+
+
+	clearWarnings ();
+
+	if (m_xResultSet.get().is())
+		clearMyResultSet();
+}
+//--------------------------------------------------------------------
+// clearMyResultSet
+// If a ResultSet was created for this Statement, close it
+//--------------------------------------------------------------------
+
+void OStatement_Base::clearMyResultSet () throw (SQLException)
+{
+	::osl::MutexGuard aGuard( m_aMutex );
+	checkDisposed(OStatement_BASE::rBHelper.bDisposed);
+
+
+	Reference<XCloseable> xCloseable;
+	if(::comphelper::query_interface(m_xResultSet.get(),xCloseable))
+		xCloseable->close();
+	m_xResultSet = Reference< XResultSet>();
+}
+
+// -------------------------------------------------------------------------
+sal_Bool SAL_CALL OStatement_Base::execute( const ::rtl::OUString& sql ) throw(SQLException, RuntimeException)
+{
+	::osl::MutexGuard aGuard( m_aMutex );
+	checkDisposed(OStatement_BASE::rBHelper.bDisposed);
+		
+	Reference< XResultSet > xRS = executeQuery( sql );
+	// returns true when a resultset is available
+	return xRS.is();
+}
+// -------------------------------------------------------------------------
+Reference< XResultSet > SAL_CALL OStatement_Base::executeQuery( const ::rtl::OUString& sql ) throw(SQLException, RuntimeException)
+{
+	::osl::MutexGuard aGuard( m_aMutex );
+	checkDisposed(OStatement_BASE::rBHelper.bDisposed);
+		
+	OEvoabResultSet* pResult = createResultSet();
+	Reference< XResultSet > xRS = pResult;
+	//	initializeResultSet( pResult );
+
+	pResult->construct();
+	m_xResultSet = xRS; // we need a reference to it fr later use
+	EVO_TRACE("LConnection m_aEvoURI: %s\n",get_utf_string(sql));
+	
+	return xRS;
+}
+// -------------------------------------------------------------------------
+
+OEvoabResultSet* OStatement_Base::createResultSet()
+{
+	return new OEvoabResultSet(this,m_pConnection->getEvoTable());
+}
+
+// -------------------------------------------------------------------------
+
+Reference< XConnection > SAL_CALL OStatement_Base::getConnection(  ) throw(SQLException, RuntimeException)
+{
+	::osl::MutexGuard aGuard( m_aMutex );
+	checkDisposed(OStatement_BASE::rBHelper.bDisposed);
+		
+	// just return our connection here
+	return (Reference< XConnection >)m_pConnection;
+}
+// -----------------------------------------------------------------------------
+// sal_Int32 SAL_CALL OStatement_Base::getUpdateCount(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException)
+// {
+// 	return 0;
+// }
+
+// -------------------------------------------------------------------------
+
+Any SAL_CALL OStatement::queryInterface( const Type & rType ) throw(RuntimeException)
+{
+	Any aRet = ::cppu::queryInterface(rType,static_cast< XServiceInfo*> (this));
+	if(!aRet.hasValue())
+		aRet = OStatement_Base::queryInterface(rType);
+	return aRet;
+}
+// -------------------------------------------------------------------------
+
+sal_Int32 SAL_CALL OStatement_Base::executeUpdate( const ::rtl::OUString& sql ) throw(SQLException, RuntimeException)
+{
+	::osl::MutexGuard aGuard( m_aMutex );
+	checkDisposed(OStatement_BASE::rBHelper.bDisposed);
+		
+	// the return values gives information about how many rows are affected by executing the sql statement		
+	return 0;
+}
+// -------------------------------------------------------------------------
+
+// Reference< XResultSet > SAL_CALL OStatement_Base::getResultSet(  ) throw(SQLException, RuntimeException)
+// {
+// 	::osl::MutexGuard aGuard( m_aMutex );
+// 	checkDisposed(OStatement_BASE::rBHelper.bDisposed);
+		
+// //	return our save resultset here
+// 	return m_xResultSet;
+// }
+
+// -------------------------------------------------------------------------
+
+// sal_Bool SAL_CALL OStatement_Base::getMoreResults(  ) throw(SQLException, RuntimeException)
+// {
+// 	::osl::MutexGuard aGuard( m_aMutex );
+// 	checkDisposed(OStatement_BASE::rBHelper.bDisposed);
+
+// 	// if your driver supports more than only one resultset 
+// 	// and has one more at this moment return true
+// 	return sal_False;
+// }
+
+// -------------------------------------------------------------------------
+
+// -------------------------------------------------------------------------
+Any SAL_CALL OStatement_Base::getWarnings(  ) throw(SQLException, RuntimeException)
+{
+	::osl::MutexGuard aGuard( m_aMutex );
+	checkDisposed(OStatement_BASE::rBHelper.bDisposed);
+		
+
+	return makeAny(m_aLastWarning);
+}
+// -------------------------------------------------------------------------
+
+// -------------------------------------------------------------------------
+void SAL_CALL OStatement_Base::clearWarnings(  ) throw(SQLException, RuntimeException)
+{
+	::osl::MutexGuard aGuard( m_aMutex );
+	checkDisposed(OStatement_BASE::rBHelper.bDisposed);
+		
+
+	m_aLastWarning = SQLWarning();
+}
+// -------------------------------------------------------------------------
+::cppu::IPropertyArrayHelper* OStatement_Base::createArrayHelper( ) const
+{
+	// this properties are define by the service statement
+	// they must in alphabetic order
+	Sequence< Property > aProps(10);
+	Property* pProperties = aProps.getArray();
+	sal_Int32 nPos = 0;
+	DECL_PROP0(CURSORNAME,	::rtl::OUString);
+	DECL_BOOL_PROP0(ESCAPEPROCESSING);
+	DECL_PROP0(FETCHDIRECTION,sal_Int32);
+	DECL_PROP0(FETCHSIZE,	sal_Int32);
+	DECL_PROP0(MAXFIELDSIZE,sal_Int32);
+	DECL_PROP0(MAXROWS,		sal_Int32);
+	DECL_PROP0(QUERYTIMEOUT,sal_Int32);
+	DECL_PROP0(RESULTSETCONCURRENCY,sal_Int32);
+	DECL_PROP0(RESULTSETTYPE,sal_Int32);
+	DECL_BOOL_PROP0(USEBOOKMARKS);
+
+	return new ::cppu::OPropertyArrayHelper(aProps);
+}
+
+// -------------------------------------------------------------------------
+::cppu::IPropertyArrayHelper & OStatement_Base::getInfoHelper()
+{
+	return *const_cast<OStatement_Base*>(this)->getArrayHelper();
+}
+// -------------------------------------------------------------------------
+sal_Bool OStatement_Base::convertFastPropertyValue(
+							Any & rConvertedValue,
+							Any & rOldValue,
+							sal_Int32 nHandle,
+							const Any& rValue )
+								throw (::com::sun::star::lang::IllegalArgumentException)
+{
+	sal_Bool bConverted = sal_False;
+	// here we have to try to convert 
+	return bConverted;
+}
+// -------------------------------------------------------------------------
+void OStatement_Base::setFastPropertyValue_NoBroadcast(sal_Int32 nHandle,const Any& rValue) throw (Exception)
+{
+	// set the value to what ever is nescessary
+	switch(nHandle)
+	{
+		case PROPERTY_ID_QUERYTIMEOUT:
+		case PROPERTY_ID_MAXFIELDSIZE:
+		case PROPERTY_ID_MAXROWS:
+		case PROPERTY_ID_CURSORNAME:
+		case PROPERTY_ID_RESULTSETCONCURRENCY:
+		case PROPERTY_ID_RESULTSETTYPE:
+		case PROPERTY_ID_FETCHDIRECTION:
+		case PROPERTY_ID_FETCHSIZE:
+		case PROPERTY_ID_ESCAPEPROCESSING:
+		case PROPERTY_ID_USEBOOKMARKS:
+		default:
+			;
+	}
+}
+// -------------------------------------------------------------------------
+void OStatement_Base::getFastPropertyValue(Any& rValue,sal_Int32 nHandle) const
+{
+	switch(nHandle)
+	{
+		case PROPERTY_ID_QUERYTIMEOUT:
+		case PROPERTY_ID_MAXFIELDSIZE:
+		case PROPERTY_ID_MAXROWS:
+		case PROPERTY_ID_CURSORNAME:
+		case PROPERTY_ID_RESULTSETCONCURRENCY:
+		case PROPERTY_ID_RESULTSETTYPE:
+		case PROPERTY_ID_FETCHDIRECTION:
+		case PROPERTY_ID_FETCHSIZE:
+		case PROPERTY_ID_ESCAPEPROCESSING:
+		case PROPERTY_ID_USEBOOKMARKS:
+		default:
+			;
+	}
+}
+// -------------------------------------------------------------------------
+IMPLEMENT_SERVICE_INFO(OStatement,"com.sun.star.sdbcx.OStatement","com.sun.star.sdbc.Statement");
+// -----------------------------------------------------------------------------
+void SAL_CALL OStatement_Base::acquire() throw()
+{
+	OStatement_BASE::acquire();
+}
+// -----------------------------------------------------------------------------
+void SAL_CALL OStatement_Base::release() throw()
+{
+	OStatement_BASE::release();
+}
+// -----------------------------------------------------------------------------
+void SAL_CALL OStatement::acquire() throw()
+{
+	OStatement_BASE2::acquire();
+}
+// -----------------------------------------------------------------------------
+void SAL_CALL OStatement::release() throw()
+{
+	OStatement_BASE2::release();
+}
+// -----------------------------------------------------------------------------
+Reference< ::com::sun::star::beans::XPropertySetInfo > SAL_CALL OStatement_Base::getPropertySetInfo(  ) throw(RuntimeException)
+{
+	return ::cppu::OPropertySetHelper::createPropertySetInfo(getInfoHelper());
+}
+// -----------------------------------------------------------------------------
+
