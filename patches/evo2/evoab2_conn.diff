--- /dev/null	2003-01-30 15:54:37.000000000 +0530
+++ connectivity/source/drivers/evoab1.5/NConnection.hxx	2004-03-18 14:55:46.000000000 +0530
@@ -0,0 +1,215 @@
+/*************************************************************************
+ *
+ *  $RCSfile$
+ *
+ *  $Revision$
+ *
+ *  last change: $Author$ $Date$
+ *
+ *  The Contents of this file are made available subject to the terms of
+ *  either of the following licenses
+ *
+ *         - GNU Lesser General Public License Version 2.1
+ *         - Sun Industry Standards Source License Version 1.1
+ *
+ *  Sun Microsystems Inc., October, 2000
+ *
+ *  GNU Lesser General Public License Version 2.1
+ *  =============================================
+ *  Copyright 2000 by Sun Microsystems, Inc.
+ *  901 San Antonio Road, Palo Alto, CA 94303, USA
+ *
+ *  This library is free software; you can redistribute it and/or
+ *  modify it under the terms of the GNU Lesser General Public
+ *  License version 2.1, as published by the Free Software Foundation.
+ *
+ *  This library is distributed in the hope that it will be useful,
+ *  but WITHOUT ANY WARRANTY; without even the implied warranty of
+ *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+ *  Lesser General Public License for more details.
+ *
+ *  You should have received a copy of the GNU Lesser General Public
+ *  License along with this library; if not, write to the Free Software
+ *  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
+ *  MA  02111-1307  USA
+ *
+ *
+ *  Sun Industry Standards Source License Version 1.1
+ *  =================================================
+ *  The contents of this file are subject to the Sun Industry Standards
+ *  Source License Version 1.1 (the "License"); You may not use this file
+ *  except in compliance with the License. You may obtain a copy of the
+ *  License at http://www.openoffice.org/license.html.
+ *
+ *  Software provided under this License is provided on an "AS IS" basis,
+ *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
+ *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
+ *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
+ *  See the License for the specific provisions governing your rights and
+ *  obligations concerning the Software.
+ *
+ *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
+ *
+ *  Copyright: 2000 by Sun Microsystems, Inc.
+ *
+ *  All Rights Reserved.
+ *
+ *  Contributor(s): _______________________________________
+ *
+ *
+ ************************************************************************/
+
+#ifndef _CONNECTIVITY_EVOAB_CONNECTION_HXX_
+#define _CONNECTIVITY_EVOAB_CONNECTION_HXX_
+
+#ifndef _CONNECTIVITY_EVOAB_DRIVER_HXX_
+#include "NDriver.hxx"
+#endif
+#ifndef _CONNECTIVITY_EVOAB_EVOCONTACTS_HXX_
+#include "NEvoContacts.hxx"
+#endif
+#ifndef _COM_SUN_STAR_SDBC_SQLWARNING_HPP_
+#include <com/sun/star/sdbc/SQLWarning.hpp>
+#endif
+#ifndef _COM_SUN_STAR_BEANS_PROPERTYVALUE_HPP_
+#include <com/sun/star/beans/PropertyValue.hpp>
+#endif
+#ifndef _CONNECTIVITY_OSUBCOMPONENT_HXX_
+#include "OSubComponent.hxx"
+#endif
+//	#include <map>
+#ifndef _COM_SUN_STAR_SDBCX_XTABLESSUPPLIER_HPP_
+#include <com/sun/star/sdbcx/XTablesSupplier.hpp>
+#endif
+#ifndef _CONNECTIVITY_COMMONTOOLS_HXX_
+#include "connectivity/CommonTools.hxx"
+#endif
+#ifndef CONNECTIVITY_CONNECTION_HXX
+#include "TConnection.hxx"
+#endif
+#ifndef _CPPUHELPER_WEAKREF_HXX_
+#include <cppuhelper/weakref.hxx>
+#endif
+#ifndef _OSL_MODULE_H_ 
+#include <osl/module.h>
+#endif
+
+
+//#ifndef _CONNECTIVITY_EVOAB_COLUMNALIAS_HXX_
+//#include "LColumnAlias.hxx"
+//#endif
+
+namespace connectivity
+{
+	namespace evoab
+	{
+
+		namespace SDBCAddress {
+			typedef enum {
+				Unknown		= 0,
+				Evolution       = 1,
+				LDAP		= 2,
+				GWISE	= 3
+			} sdbc_address_type;
+		}
+		
+		typedef connectivity::OMetaConnection				OConnection_BASE; // implements basics and text encoding
+		
+		class OEvoabConnection : public OConnection_BASE,
+					 public connectivity::OSubComponent<OEvoabConnection, OConnection_BASE>
+		{
+
+			friend class connectivity::OSubComponent<OEvoabConnection, OConnection_BASE>;
+
+		private:	
+			//OColumnAlias	m_aColumnAlias;
+			sal_Bool		m_bFixedLength;			// row of fixed length
+			sal_Bool		m_bHeaderLine; 			// column names in first row
+			sal_Unicode		m_cFieldDelimiter;		// look at the name
+			sal_Unicode		m_cStringDelimiter;		// delimiter for strings m_cStringDelimiter blabla m_cStringDelimiter
+			sal_Unicode		m_cDecimalDelimiter;	// Dezimal-delimiter (Dezimalpoint)
+			sal_Unicode		m_cThousandDelimiter;	// 
+			sal_Int32		m_nMaxResultRecords;
+			rtl::OUString	m_aEvoURI;
+			rtl::OUString   m_aUserName;
+			rtl::OUString   m_aPassword;
+			OEvoabDriver*							m_pDriver;		//	Pointer to the owning
+			EvoContacts                                                     *m_pTable;
+			
+			SDBCAddress::sdbc_address_type  m_eSDBCAddressType;
+		      
+		protected:
+
+			::com::sun::star::uno::WeakReference< ::com::sun::star::sdbc::XDatabaseMetaData > m_xMetaData;
+			
+			connectivity::OWeakRefArray				m_aStatements;	//	vector containing a list
+			//  of all the Statement objects
+			//  for this Connection
+
+			
+		public:
+			OEvoabConnection(OEvoabDriver*	_pDriver);
+			virtual ~OEvoabConnection();
+			
+			virtual void construct(const ::rtl::OUString& _rUrl,const ::com::sun::star::uno::Sequence< ::com::sun::star::beans::PropertyValue >& _rInfo ) throw( ::com::sun::star::sdbc::SQLException);
+			
+			// own methods
+			inline const OEvoabDriver*	getDriver()				const { return static_cast< const OEvoabDriver* >( m_pDriver );			}
+			inline sal_Bool			isFixedLength()				const { return m_bFixedLength;		}
+			inline sal_Bool			isHeaderLine()				const { return m_bHeaderLine;		}
+			inline sal_Unicode		getFieldDelimiter()			const { return m_cFieldDelimiter;	}
+			inline sal_Unicode		getStringDelimiter()			const { return m_cStringDelimiter;	}
+			inline sal_Unicode		getDecimalDelimiter()			const { return m_cDecimalDelimiter; }
+			inline sal_Unicode		getThousandDelimiter()			const { return m_cThousandDelimiter;}
+			//const OColumnAlias& 	getColumnAlias() 				const { return m_aColumnAlias; }
+			
+			
+			SDBCAddress::sdbc_address_type getSDBCAddressType() const { return m_eSDBCAddressType;}
+			//sal_Bool queryURI() const { return (m_eSDBCAddressType == SDBCAddress::LDAP) || (m_eSDBCAddressType == SDBCAddress::Groupwise); }
+			
+			::rtl::OUString getEvoURI(){ return m_aEvoURI; }
+			::rtl::OUString getEvoUserName(){ return m_aUserName; }
+			::rtl::OUString getEvoPasswd(){ return m_aPassword; }
+			
+			EvoContacts* getEvoTable(){ return m_pTable; }
+			
+			// OComponentHelper
+			virtual void SAL_CALL disposing(void);
+			// XInterface
+			virtual void SAL_CALL release() throw();
+			
+			// XServiceInfo
+			DECLARE_SERVICE_INFO();
+			
+			// XConnection
+			//			virtual ::com::sun::star::uno::Reference< ::com::sun::star::sdbcx::XTablesSupplier > createCatalog();
+			virtual ::com::sun::star::uno::Reference< ::com::sun::star::sdbc::XStatement > SAL_CALL createStatement(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual ::com::sun::star::uno::Reference< ::com::sun::star::sdbc::XPreparedStatement > SAL_CALL prepareStatement( const ::rtl::OUString& sql ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual ::com::sun::star::uno::Reference< ::com::sun::star::sdbc::XPreparedStatement > SAL_CALL prepareCall( const ::rtl::OUString& sql ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual ::rtl::OUString SAL_CALL nativeSQL( const ::rtl::OUString& sql ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual void SAL_CALL setAutoCommit( sal_Bool autoCommit ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual sal_Bool SAL_CALL getAutoCommit(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual void SAL_CALL commit(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual void SAL_CALL rollback(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual sal_Bool SAL_CALL isClosed(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual ::com::sun::star::uno::Reference< ::com::sun::star::sdbc::XDatabaseMetaData > SAL_CALL getMetaData(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual void SAL_CALL setReadOnly( sal_Bool readOnly ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual sal_Bool SAL_CALL isReadOnly(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual void SAL_CALL setCatalog( const ::rtl::OUString& catalog ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual ::rtl::OUString SAL_CALL getCatalog(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual void SAL_CALL setTransactionIsolation( sal_Int32 level ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual sal_Int32 SAL_CALL getTransactionIsolation(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual ::com::sun::star::uno::Reference< ::com::sun::star::container::XNameAccess > SAL_CALL getTypeMap(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual void SAL_CALL setTypeMap( const ::com::sun::star::uno::Reference< ::com::sun::star::container::XNameAccess >& typeMap ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			
+			// XCloseable
+			virtual void SAL_CALL close(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			// XWarningsSupplier
+			virtual ::com::sun::star::uno::Any SAL_CALL getWarnings(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			virtual void SAL_CALL clearWarnings(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
+			
+			
+		};
+	}
+}
+#endif // _CONNECTIVITY_EVOAB_CONNECTION_HXX_
--- /dev/null	2003-01-30 15:54:37.000000000 +0530
+++ connectivity/source/drivers/evoab1.5/NConnection.cxx	2004-03-18 14:55:29.000000000 +0530
@@ -0,0 +1,646 @@
+/*************************************************************************
+ *
+ *  $RCSfile$
+ *
+ *  $Revision$
+ *
+ *  last change: $Author$ $Date$
+ *
+ *  The Contents of this file are made available subject to the terms of
+ *  either of the following licenses
+ *
+ *         - GNU Lesser General Public License Version 2.1
+ *         - Sun Industry Standards Source License Version 1.1
+ *
+ *  Sun Microsystems Inc., October, 2000
+ *
+ *  GNU Lesser General Public License Version 2.1
+ *  =============================================
+ *  Copyright 2000 by Sun Microsystems, Inc.
+ *  901 San Antonio Road, Palo Alto, CA 94303, USA
+ *
+ *  This library is free software; you can redistribute it and/or
+ *  modify it under the terms of the GNU Lesser General Public
+ *  License version 2.1, as published by the Free Software Foundation.
+ *
+ *  This library is distributed in the hope that it will be useful,
+ *  but WITHOUT ANY WARRANTY; without even the implied warranty of
+ *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+ *  Lesser General Public License for more details.
+ *
+ *  You should have received a copy of the GNU Lesser General Public
+ *  License along with this library; if not, write to the Free Software
+ *  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
+ *  MA  02111-1307  USA
+ *
+ *
+ *  Sun Industry Standards Source License Version 1.1
+ *  =================================================
+ *  The contents of this file are subject to the Sun Industry Standards
+ *  Source License Version 1.1 (the "License"); You may not use this file
+ *  except in compliance with the License. You may obtain a copy of the
+ *  License at http://www.openoffice.org/license.html.
+ *
+ *  Software provided under this License is provided on an "AS IS" basis,
+ *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
+ *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
+ *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
+ *  See the License for the specific provisions governing your rights and
+ *  obligations concerning the Software.
+ *
+ *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
+ *
+ *  Copyright: 2000 by Sun Microsystems, Inc.
+ *
+ *  All Rights Reserved.
+ *
+ *  Contributor(s): _______________________________________
+ *
+ *
+ ************************************************************************/
+ 
+#ifndef _CONNECTIVITY_EVOAB_CONNECTION_HXX_
+#include "NConnection.hxx"
+#endif
+#ifndef _CONNECTIVITY_EVOAB_DATABASEMETADATA_HXX_
+#include "NDatabaseMetaData.hxx"
+#endif
+//#ifndef _CONNECTIVITY_EVOAB_CATALOG_HXX_
+//#include "LCatalog.hxx"
+//#endif
+#ifndef _CONNECTIVITY_RESOURCE_HRC_
+#include "Resource.hrc"
+#endif
+#ifndef _CONNECTIVITY_MODULECONTEXT_HXX_
+#include "ModuleContext.hxx"
+#endif
+#ifndef _COM_SUN_STAR_LANG_DISPOSEDEXCEPTION_HPP_
+#include <com/sun/star/lang/DisposedException.hpp>
+#endif
+#ifndef _COM_SUN_STAR_SDBC_TRANSACTIONISOLATION_HPP_
+#include <com/sun/star/sdbc/TransactionIsolation.hpp>
+#endif
+#ifndef _URLOBJ_HXX //autogen wg. INetURLObject
+#include <tools/urlobj.hxx>
+#endif
+#ifndef _CONNECTIVITY_EVOAB_PREPAREDSTATEMENT_HXX_
+#include "NPreparedStatement.hxx"
+#endif
+#ifndef _CONNECTIVITY_EVOAB_STATEMENT_HXX_
+#include "NStatement.hxx"
+#endif
+#ifndef _COMPHELPER_EXTRACT_HXX_
+#include <comphelper/extract.hxx>
+#endif
+#ifndef _DBHELPER_DBEXCEPTION_HXX_
+#include <connectivity/dbexception.hxx>
+#endif
+#ifndef _COMPHELPER_PROCESSFACTORY_HXX_
+#include <comphelper/processfactory.hxx>
+#endif
+#ifndef _VOS_PROCESS_HXX_
+#include <vos/process.hxx>
+#endif
+#ifndef _TOOLS_DEBUG_HXX
+#include <tools/debug.hxx>
+#endif
+#ifndef CONNECTIVITY_EVOAB_DEBUG_HELPER_HXX
+#include "NDebug.hxx"
+#endif
+#ifndef _COMPHELPER_SEQUENCE_HXX_
+#include <comphelper/sequence.hxx>
+#endif
+
+using namespace connectivity::evoab;
+using namespace vos;
+using namespace dbtools;
+
+//typedef connectivity::file::OConnection  OConnection_B;
+
+//------------------------------------------------------------------------------
+using namespace ::com::sun::star::uno;
+using namespace ::com::sun::star::beans;
+using namespace ::com::sun::star::sdbcx;
+using namespace ::com::sun::star::sdbc;
+using namespace ::com::sun::star::lang;
+
+namespace connectivity {
+    namespace evoab {
+		// For the moment, we will connect the Evol address book to the Mozilla
+		// top-level address book which will display whatever is in the preferences
+		// file of Mozilla.
+        static sal_Char*    EVO_SCHEME_EVOLUTION          = "evolution://";
+		// This one is a base uri which will be completed with the connection data.
+        static sal_Char*    EVO_SCHEME_LDAP             = "ldap://";
+		// These two uris will be used to obtain directory factories to access all
+		// address books of the given type.
+        static sal_Char*    EVO_SCHEME_GWISE     = "groupwise://";
+    }
+}
+
+
+::rtl::OUString implGetExceptionMsg( Exception& e, const ::rtl::OUString& aExceptionType_ )
+{
+     ::rtl::OUString aExceptionType = aExceptionType_;
+     if( aExceptionType.getLength() == 0 )
+         aExceptionType = ::rtl::OUString( ::rtl::OUString::createFromAscii("Unknown" ) );
+ 
+     ::rtl::OUString aTypeLine( ::rtl::OUString::createFromAscii("\nType: " ) );
+     aTypeLine += aExceptionType;
+ 
+     ::rtl::OUString aMessageLine( ::rtl::OUString::createFromAscii("\nMessage: " ) );
+         aMessageLine += ::rtl::OUString( e.Message );
+ 
+	 ::rtl::OUString aMsg(aTypeLine);
+     aMsg += aMessageLine;
+         return aMsg;
+}
+ 
+ // Exception type unknown
+::rtl::OUString implGetExceptionMsg( Exception& e )
+{
+         ::rtl::OUString aMsg = implGetExceptionMsg( e, ::rtl::OUString() );
+         return aMsg;
+}
+
+// --------------------------------------------------------------------------------
+OEvoabConnection::OEvoabConnection(OEvoabDriver*	_pDriver) :
+	OSubComponent<OEvoabConnection, OConnection_BASE>((::cppu::OWeakObject*)_pDriver, this)
+	,m_pDriver(_pDriver)
+	,m_pTable(NULL)
+	,m_bFixedLength(sal_False)
+	,m_bHeaderLine(sal_True)
+	,m_cFieldDelimiter(',')
+	,m_cStringDelimiter('"')
+	,m_cDecimalDelimiter('.')
+	,m_cThousandDelimiter(' ')
+{
+	m_pDriver->acquire();
+	// Initialise m_aColumnAlias.
+	//	m_aColumnAlias.setAlias(_pDriver->getFactory());
+}
+//-----------------------------------------------------------------------------
+OEvoabConnection::~OEvoabConnection()
+{
+	if(!isClosed())
+		close();
+	
+
+	m_pDriver->release();
+	m_pDriver = NULL;
+	delete m_pTable;
+}
+
+//-----------------------------------------------------------------------------
+void SAL_CALL OEvoabConnection::release() throw()
+{
+	relase_ChildImpl();
+}
+
+// XServiceInfo
+// --------------------------------------------------------------------------------
+IMPLEMENT_SERVICE_INFO(OEvoabConnection, "com.sun.star.sdbc.drivers.evoab.Connection", "com.sun.star.sdbc.Connection")
+
+//-----------------------------------------------------------------------------
+void OEvoabConnection::construct(const ::rtl::OUString& url,const Sequence< PropertyValue >& info)  throw(SQLException)
+{
+	osl_incrementInterlockedCount( &m_refCount );
+	EVO_TRACE_STRING("OEvoabConnection::construct()::url = %s\n", url );
+
+
+	/* get the evo schema properties based on the kind of urls for example ldap will need user name and password etc and set it here 
+	   as shown below */
+
+	OSL_TRACE("IN OConnection::construct()\n" );
+	setURL(url);
+
+	const PropertyValue* pInfoD = info.getConstArray();
+	const PropertyValue* pInfoEndD = pInfoD + info.getLength();		
+	for (; pInfoD != pInfoEndD; ++pInfoD)
+		{
+			rtl::OUString Name;
+			pInfoD->Value >>= Name;
+			fprintf(stderr,"pinfoD->Name: %s %s\n",get_utf_string(pInfoD->Name),get_utf_string(Name));
+		}
+	
+
+
+	//
+	// Skip 'sdbc:address: part of URL
+	//
+	sal_Int32 nLen = url.indexOf(':');
+	nLen = url.indexOf(':',nLen+1);
+	::rtl::OUString aAddrbookURI(url.copy(nLen+1));
+	// Get Scheme
+	nLen = aAddrbookURI.indexOf(':');
+	::rtl::OUString aAddrbookScheme;
+	if ( nLen == -1 )
+	{
+		// There isn't any subschema: - but could be just subschema
+		if ( aAddrbookURI.getLength() > 0 ) {
+			aAddrbookScheme= aAddrbookURI;
+		}
+		else {
+			OSL_TRACE( "No subschema given!!!\n");
+			::dbtools::throwGenericSQLException(
+							    ::rtl::OUString::createFromAscii("No subschema provided"),NULL);
+		}
+	}
+	else {
+		aAddrbookScheme = aAddrbookURI.copy(0, nLen);
+	}
+
+	printf("URI = %s\n", get_utf_string(aAddrbookURI) );
+	printf("Scheme = %s\n", get_utf_string(aAddrbookScheme) );
+
+	//
+	// Now we have a URI convert it to a Evolution URI
+	//
+	// The Mapping being used is:
+	//
+	// * for Evolution
+	//      "sdbc:address:evolution:"        -> flat://
+	// * for LDAP
+	//      "sdbc:address:ldap:"           ->  ldap://
+	// * for Groupwise
+	//      "sdbc:address:groupwise:"        -> groupwise://
+
+
+	/* Even if uri is ldap|groupwise still make a flat file uri to sent to OFileConnection otherwise it will crib */
+// 	rtl::OUString aEvoFlatURI;
+// 	aEvoFlatURI = ::rtl::OUString(RTL_CONSTASCII_USTRINGPARAM( "sdbc:flat:" ));
+// 	aEvoFlatURI += getDriver()->getWorkingDirURL();
+// 	EVO_TRACE_STRING("OEvoabConnection::construct()::m_aEvoFlatURI = %s\n", aEvoFlatURI );
+// 	printf("aEvoFlatURI string %s\n",get_utf_string(aEvoFlatURI));
+
+	if ( aAddrbookScheme.compareToAscii( OEvoabDriver::getSDBC_SCHEME_EVOLUTION() ) == 0 ) {
+		// set uri to null 
+		//		m_aEvoURI(NULL
+		m_aEvoURI = rtl::OUString::createFromAscii( EVO_SCHEME_EVOLUTION );
+		m_eSDBCAddressType = SDBCAddress::Evolution;
+	}
+	else if ( aAddrbookScheme.compareToAscii( OEvoabDriver::getSDBC_SCHEME_LDAP() ) == 0 ) {
+		rtl::OUString sHostName;
+		rtl::OUString sBaseDN;
+		rtl::OUString sPasswd;
+		sal_Int32     nPortNumber = -1;
+		
+		m_aEvoURI = rtl::OUString::createFromAscii( EVO_SCHEME_LDAP );
+		
+		const PropertyValue* pInfo = info.getConstArray();
+		const PropertyValue* pInfoEnd = pInfo + info.getLength();
+
+
+
+		m_eSDBCAddressType = SDBCAddress::LDAP;
+		
+		for (; pInfo != pInfoEnd; ++pInfo)
+			{
+				if ( 0 == pInfo->Name.compareToAscii("HostName") )
+					{
+						pInfo->Value >>= sHostName;
+					}
+				else if ( 0 == pInfo->Name.compareToAscii("BaseDN") )
+					{
+						pInfo->Value >>= sBaseDN;
+					}
+				else if ( 0 == pInfo->Name.compareToAscii("PortNumber") )
+					{
+						pInfo->Value >>= nPortNumber;
+					}
+				else if ( 0 == pInfo->Name.compareToAscii("MaxRowCount") )
+					{
+						//pInfo->Value >>= m_nMaxResultRecords;
+					}
+			}
+		if ( sHostName.getLength() != 0 ) {
+			m_aEvoURI += sHostName;
+		}
+		else {
+			::dbtools::throwGenericSQLException(
+							    ::rtl::OUString::createFromAscii("No HostName provided"),NULL);
+		}
+		
+		if ( nPortNumber > 0 ) {
+			m_aEvoURI += rtl::OUString::createFromAscii( ":" );
+			m_aEvoURI += rtl::OUString::valueOf( nPortNumber );
+		}
+		
+		if ( sBaseDN.getLength() != 0 ) {
+			m_aEvoURI += rtl::OUString::createFromAscii( "/" );
+			m_aEvoURI += sBaseDN;
+		}
+		else {
+			::dbtools::throwGenericSQLException(
+							    ::rtl::OUString::createFromAscii("No BaseDN provided"),NULL);
+		}
+		// Addition of a fake query to enable the LDAP directory to work correctly.
+		m_aEvoURI += ::rtl::OUString::createFromAscii("??sub?(*)");
+
+	}
+	else if ( aAddrbookScheme.compareToAscii( OEvoabDriver::getSDBC_SCHEME_GWISE() ) == 0 ) {
+		rtl::OUString sHostName;
+		rtl::OUString sUserName;
+		rtl::OUString sPasswd;
+		sal_Int32     nPortNumber = -1;
+		
+		m_aEvoURI = rtl::OUString::createFromAscii( EVO_SCHEME_GWISE );
+		
+		const PropertyValue* pInfo = info.getConstArray();
+		const PropertyValue* pInfoEnd = pInfo + info.getLength();
+
+		
+		m_eSDBCAddressType = SDBCAddress::GWISE;
+		
+		for (; pInfo != pInfoEnd; ++pInfo)
+			{
+				if ( 0 == pInfo->Name.compareToAscii("HostName") )
+					{
+						pInfo->Value >>= sHostName;
+					}
+				//	else if ( 0 == pInfo->Name.compareToAscii("user") )
+				//{
+				//	pInfo->Value >>= m_aUserName;
+				//}
+				//else if ( 0 == pInfo->Name.compareToAscii("password") )
+				//{
+				//	pInfo->Value >>= m_aPassword;
+				//}
+				else if ( 0 == pInfo->Name.compareToAscii("PortNumber") )
+					{
+						pInfo->Value >>= nPortNumber;
+					}
+				else if ( 0 == pInfo->Name.compareToAscii("MaxRowCount") )
+					{
+						//pInfo->Value >>= m_nMaxResultRecords;
+					}
+			}
+		if ( sHostName.getLength() != 0 ) {
+			m_aEvoURI += sHostName;
+			printf("LConnection m_aEvoURI: %s\n",get_utf_string(m_aEvoURI));
+		}
+		else {
+			::dbtools::throwGenericSQLException(
+							    ::rtl::OUString::createFromAscii("No HostName provided"),NULL);
+		}
+		
+		if ( nPortNumber < 0 ) {
+			
+			printf("No port number, trying default\n");
+			// try default port
+			nPortNumber = 7181;
+		}
+
+		m_aEvoURI += rtl::OUString::createFromAscii( ":" );
+		m_aEvoURI += rtl::OUString::valueOf( nPortNumber );
+		m_aEvoURI += rtl::OUString::createFromAscii( "/" );
+		m_aEvoURI += rtl::OUString::createFromAscii( "soap" );
+		printf("LConnection m_aEvoURI: %s\n",get_utf_string(m_aEvoURI));
+
+	}
+	else
+		{
+			OSL_TRACE("Invalid subschema given!!!\n");
+			::dbtools::throwGenericSQLException(
+							    ::rtl::OUString::createFromAscii("Invalid subschema provided"),NULL);
+		}
+	
+	Sequence<PropertyValue> aDriverParam;   
+	::std::vector<PropertyValue> aParam;
+	
+	aParam.push_back(PropertyValue(::rtl::OUString::createFromAscii("EnableSQL92Check"), 0, Any(), PropertyState_DIRECT_VALUE));
+	aParam.push_back(PropertyValue(::rtl::OUString::createFromAscii("CharSet"), 0, Any(), PropertyState_DIRECT_VALUE));
+	aParam.push_back(PropertyValue(::rtl::OUString::createFromAscii("HeaderLine"), 0, makeAny(m_bHeaderLine), PropertyState_DIRECT_VALUE));
+	aParam.push_back(PropertyValue(::rtl::OUString::createFromAscii("FieldDelimiter"), 0, makeAny(::rtl::OUString(&m_cFieldDelimiter,1)), PropertyState_DIRECT_VALUE));
+	aParam.push_back(PropertyValue(::rtl::OUString::createFromAscii("StringDelimiter"), 0, makeAny(::rtl::OUString(&m_cStringDelimiter,1)), PropertyState_DIRECT_VALUE));
+	aParam.push_back(PropertyValue(::rtl::OUString::createFromAscii("DecimalDelimiter"), 0, makeAny(::rtl::OUString(&m_cDecimalDelimiter,1)), PropertyState_DIRECT_VALUE));
+	aParam.push_back(PropertyValue(::rtl::OUString::createFromAscii("ThousandDelimiter"), 0, makeAny(::rtl::OUString(&m_cThousandDelimiter,1)), PropertyState_DIRECT_VALUE));
+
+	// build a new parameter sequence from the original parameters, appended b� the new parameters from above
+	aDriverParam = ::comphelper::concatSequences(
+						     info,
+						     Sequence< PropertyValue >( aParam.begin(),aParam.size() )
+						     );
+	
+	// transform "sdbc:address:evolution" part of URL to "sdbc:flat:file:///..."
+	//
+	//
+	// Now we have a URI convert it to a Evolution CLI flat file URI
+	//
+	// The Mapping being used is:
+	//
+	// * for Evolution
+	//      "sdbc:address:evolution:"        -> "sdbc:flat:file:///(file path generated)
+	
+	
+	printf("LConnection m_aEvoURI: %s\n",get_utf_string(m_aEvoURI));
+
+	/* Try to connect to the backend, throw an exception if not possible */
+	if (m_eSDBCAddressType == SDBCAddress::GWISE)
+		m_pTable = new EvoContacts(get_utf_string(m_aEvoURI),get_utf_string(m_aUserName),get_utf_string(m_aPassword));
+	else
+		m_pTable = new EvoContacts(get_utf_string(m_aEvoURI));
+
+	// destry the password its dangerous to cache password 
+
+	if ( m_pTable->construct() == -1 ){
+		::dbtools::throwGenericSQLException(::rtl::OUString::createFromAscii("Error connecting to the database"), NULL);
+	}
+        
+	osl_decrementInterlockedCount( &m_refCount );
+}
+
+// --------------------------------------------------------------------------------
+::rtl::OUString SAL_CALL OEvoabConnection::nativeSQL( const ::rtl::OUString& _sSql ) throw(SQLException, RuntimeException)
+{
+	::osl::MutexGuard aGuard( m_aMutex );
+	// when you need to transform SQL92 to you driver specific you can do it here
+	//OSL_TRACE("OEvoabConnection::nativeSQL( %s )", OUtoCStr( _sSql ) );
+
+	return _sSql;
+}
+// --------------------------------------------------------------------------------
+Reference< XDatabaseMetaData > SAL_CALL OEvoabConnection::getMetaData(  ) throw(SQLException, RuntimeException)
+{
+	::osl::MutexGuard aGuard( m_aMutex );
+	checkDisposed(OConnection_BASE::rBHelper.bDisposed);
+		
+
+	Reference< XDatabaseMetaData > xMetaData = m_xMetaData;
+	if(!xMetaData.is())
+	{
+		xMetaData = new OEvoabDatabaseMetaData(this);
+		m_xMetaData = xMetaData;
+	}
+
+	return xMetaData;
+}
+//------------------------------------------------------------------------------
+// ::com::sun::star::uno::Reference< XTablesSupplier > OEvoabConnection::createCatalog()
+// {
+// 	::osl::MutexGuard aGuard( m_aMutex );
+// 	Reference< XTablesSupplier > xTab = m_xCatalog;
+// 	if(!xTab.is())
+// 	{
+// 		OEvoabCatalog *pCat = new OEvoabCatalog(this);
+// 		xTab = pCat;
+// 		m_xCatalog = xTab;
+// 	}
+// 	return xTab;
+// }
+// --------------------------------------------------------------------------------
+Reference< XStatement > SAL_CALL OEvoabConnection::createStatement(  ) throw(SQLException, RuntimeException)
+{
+	::osl::MutexGuard aGuard( m_aMutex );
+	checkDisposed(OConnection_BASE::rBHelper.bDisposed);
+        
+	OStatement* pStmt = new OStatement(this);
+
+	Reference< XStatement > xStmt = pStmt;
+	m_aStatements.push_back(WeakReferenceHelper(*pStmt));
+	return xStmt;
+}
+// --------------------------------------------------------------------------------
+Reference< XPreparedStatement > SAL_CALL OEvoabConnection::prepareStatement( const ::rtl::OUString& sql ) throw(SQLException, RuntimeException)
+{
+	::osl::MutexGuard aGuard( m_aMutex );
+	checkDisposed(OConnection_BASE::rBHelper.bDisposed);
+        
+
+	OEvoabPreparedStatement* pStmt = new OEvoabPreparedStatement(this, sql);
+	Reference< XPreparedStatement > xStmt = pStmt;
+	
+	m_aStatements.push_back(WeakReferenceHelper(*pStmt));
+	return xStmt;
+}
+// --------------------------------------------------------------------------------
+Reference< XPreparedStatement > SAL_CALL OEvoabConnection::prepareCall( const ::rtl::OUString& sql ) throw(SQLException, RuntimeException)
+{
+	::osl::MutexGuard aGuard( m_aMutex );
+	checkDisposed(OConnection_BASE::rBHelper.bDisposed);
+        
+	return NULL;
+}
+// --------------------------------------------------------------------------------
+void SAL_CALL OEvoabConnection::setAutoCommit( sal_Bool autoCommit ) throw(SQLException, RuntimeException)
+{
+	// here you  have to set your commit mode please have a look at the jdbc documentation to get a clear explanation
+}
+// --------------------------------------------------------------------------------
+sal_Bool SAL_CALL OEvoabConnection::getAutoCommit(  ) throw(SQLException, RuntimeException)
+{
+	// you have to distinguish which if you are in autocommit mode or not
+	// at normal case true should be fine here
+
+	return sal_True;
+}
+// --------------------------------------------------------------------------------
+void SAL_CALL OEvoabConnection::commit(  ) throw(SQLException, RuntimeException)
+{
+	// when you database does support transactions you should commit here
+}
+// --------------------------------------------------------------------------------
+void SAL_CALL OEvoabConnection::rollback(  ) throw(SQLException, RuntimeException)
+{
+	// same as commit but for the other case
+}
+// --------------------------------------------------------------------------------
+sal_Bool SAL_CALL OEvoabConnection::isClosed(  ) throw(SQLException, RuntimeException)
+{
+	::osl::MutexGuard aGuard( m_aMutex );
+
+	// just simple -> we are close when we are disposed taht means someone called dispose(); (XComponent)
+	return OConnection_BASE::rBHelper.bDisposed;
+}
+// -------------------------------------------------------------------------
+void SAL_CALL OEvoabConnection::setReadOnly( sal_Bool readOnly ) throw(SQLException, RuntimeException)
+{
+	// set you connection to readonly
+}
+// --------------------------------------------------------------------------------
+sal_Bool SAL_CALL OEvoabConnection::isReadOnly(  ) throw(SQLException, RuntimeException)
+{
+	// return if your connection to readonly
+	return sal_False;
+}
+// --------------------------------------------------------------------------------
+void SAL_CALL OEvoabConnection::setCatalog( const ::rtl::OUString& catalog ) throw(SQLException, RuntimeException)
+{
+	// if your database doesn't work with catalogs you go to next method otherwise you kjnow what to do
+}
+// --------------------------------------------------------------------------------
+::rtl::OUString SAL_CALL OEvoabConnection::getCatalog(  ) throw(SQLException, RuntimeException)
+{
+	// return your current catalog
+	return ::rtl::OUString();
+}
+// --------------------------------------------------------------------------------
+void SAL_CALL OEvoabConnection::setTransactionIsolation( sal_Int32 level ) throw(SQLException, RuntimeException)
+{
+	// set your isolation level
+	// please have a look at @see com.sun.star.sdbc.TransactionIsolation
+}
+// --------------------------------------------------------------------------------
+sal_Int32 SAL_CALL OEvoabConnection::getTransactionIsolation(  ) throw(SQLException, RuntimeException)
+{
+	// please have a look at @see com.sun.star.sdbc.TransactionIsolation
+	return TransactionIsolation::NONE;
+}
+// --------------------------------------------------------------------------------
+Reference< ::com::sun::star::container::XNameAccess > SAL_CALL OEvoabConnection::getTypeMap(  ) throw(SQLException, RuntimeException)
+{
+	// if your driver has special database types you can return it here
+	return NULL;
+}
+// --------------------------------------------------------------------------------
+void SAL_CALL OEvoabConnection::setTypeMap( const Reference< ::com::sun::star::container::XNameAccess >& typeMap ) throw(SQLException, RuntimeException)
+{
+	// the other way around
+}
+
+// --------------------------------------------------------------------------------
+// XCloseable
+void SAL_CALL OEvoabConnection::close(  ) throw(SQLException, RuntimeException)
+{
+	// we just dispose us
+	{
+		::osl::MutexGuard aGuard( m_aMutex );
+		checkDisposed(OConnection_BASE::rBHelper.bDisposed);
+
+	}
+	dispose();
+}
+
+// --------------------------------------------------------------------------------
+// XWarningsSupplier
+Any SAL_CALL OEvoabConnection::getWarnings(  ) throw(SQLException, RuntimeException)
+{
+	// when you collected some warnings -> return it
+	return Any();
+}
+// --------------------------------------------------------------------------------
+void SAL_CALL OEvoabConnection::clearWarnings(  ) throw(SQLException, RuntimeException)
+{
+	// you should clear your collected warnings here
+}
+//------------------------------------------------------------------------------
+void OEvoabConnection::disposing()
+{
+	// we noticed that we should be destroied in near future so we have to dispose our statements
+	::osl::MutexGuard aGuard(m_aMutex);
+
+	for (OWeakRefArray::iterator i = m_aStatements.begin(); m_aStatements.end() != i; ++i)
+	{
+		Reference< XComponent > xComp(i->get(), UNO_QUERY);
+		if (xComp.is())
+			xComp->dispose();
+	}
+	m_aStatements.clear();
+
+	m_xMetaData = ::com::sun::star::uno::WeakReference< ::com::sun::star::sdbc::XDatabaseMetaData>();
+
+	dispose_ChildImpl();
+	OConnection_BASE::disposing();
+}
+// -----------------------------------------------------------------------------
+
