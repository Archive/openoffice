AC_INIT(bin/build-ooo)
AC_PREREQ(2.12)
AC_PREFIX_DEFAULT(/usr)

OOO_BUILDMAJOR_VERSION=1
OOO_BUILDMINOR_VERSION=1
OOO_BUILDMICRO_VERSION=55
dnl this includes pre-pended period.
OOO_BUILDSTRIKE_VERSION=
OOO_BUILDVERSION="$OOO_BUILDMAJOR_VERSION.$OOO_BUILDMINOR_VERSION.$OOO_BUILDMICRO_VERSION$OOO_BUILDSTRIKE_VERSION"

AM_INIT_AUTOMAKE(ooo-build, $OOO_BUILDVERSION)

dnl for strike versioning
OOO_BUILD_EXTENSION="(build $OOO_BUILDMICRO_VERSION$OOO_BUILDSTRIKE_VERSION)"
AC_SUBST(OOO_BUILD_EXTENSION)

AC_ARG_WITH(tag,
[
	--with-tag: define the tag to use, defaults to the latest
			release; eg. --with-tag=OOO_1_0_3
],,)

AC_ARG_WITH(srcdir,
[
	--with-srcdir: define the directory containing the rest of the
			source code we need to build.
],,)

AC_ARG_WITH(win32,
[
	--with-win32: define the build to be on win32; this sets a
		      number of defaults suitable for that platform.
],,)

AC_ARG_WITH(system-gcc,
[
	--with-system-gcc: use the systems gcc / binutils instead of
			building our own.
],,)

AC_ARG_WITH(distro,
[
	--with-distro:	build with a specific distributions patch-set
			values are: [Ximian], Debian, ...
],,)

AC_ARG_ENABLE(devel,
[
	--enable-devel: build the latest bleeding edge untested bleeding
			edge, un-buildable set of patches.
],,)

AC_ARG_ENABLE(bonobo,
[
	--enable-bonobo: build the ooo bonobo component.
],,)

AC_ARG_WITH(ooo-builddir,
[
	--with-ooo-builddir: define the directory where openoffice.org will be
			compiled, e.g. the root of an ooo cvs checkout.
],,)

AC_ARG_ENABLE(java,
[
	--enable-java:    build ooo with a Jdk & Java support
],,)

AC_ARG_WITH(jdk-home,
[
	--with-jdk-home: specify the path of your JDK.  If you enable Java
			but do not specify the JDK path, $BUILD_DIR/jdk/bin is assumed. If
			Java is disabled, this option has no effect.
],,)

AC_ARG_WITH(installed-ooo-dirname,
[
	--with-installed-ooo-dirname:	specify the directory name of the core OOo network
			install dir, for example "openoffice" or "ooo-1.1", yielding a core install
			directory of /usr/lib/openoffice or /usr/lib/ooo-1.1.
				Example:  --with-installed-ooo-dirname=ooo-1.1
],,)

AC_ARG_WITH(vendor,
[
	--with-vendor:	specify an overall Vendor name to simplify configuration and packaging
			when a vendor does the same thing for multiple Distributions
],,)

AC_ARG_WITH(num-cpus,
[
	--with-num-cpus: # of build processes/cpus to use.  Multi-process/multi-cpu builds can
			save a lot of time on multi-cpu machines.
],,)

AC_ARG_WITH(ccache-allowed,
[
	--with-ccache-allowed: respect the setting of CC and CXX to allow building with ccache
],,)

AC_ARG_WITH(icons,
[
	--with-icons:	specify the iconset to be used.
			Possible values: Ximian (default), KDE, KDE_debug, Bluecurve.
				Example:  --with-icons=Ximian
],,)


AM_MAINTAINER_MODE

dnl
dnl Setup the misc. tweaks we need.
dnl
BASEDIR=`pwd`
TOOLSDIR=$BASEDIR
AC_SUBST(BASEDIR)
AC_SUBST(TOOLSDIR)

AC_PATH_PROG(PERL, perl)
if test -z "PERL"; then
   AC_MSG_ERROR([perl not found; required for ooo-build])
fi
AC_SUBST(PERL)
INTLTOOL_PERL=$PERL
AC_SUBST(INTLTOOL_PERL)

AC_PATH_PROG(PKG_CONFIG, pkg-config)
if test ! -x "$PKG_CONFIG" ; then
   AC_MSG_ERROR([
*** You need the latest pkg-config.
*** Get the latest version of pkg-config from
*** <http://www.freedesktop.org/software/pkgconfig/>.])
fi
AC_SUBST(PKG_CONFIG)

if test "z$enable_devel" = "z"; then
   latest_flags=
else
   latest_flags=--devel
fi

PKG_CHECK_MODULES( FOO_COMMON, 
		   [ gtk+-2.0 libxml-2.0 fontconfig >= 1.0.1 \
		   libstartup-notification-1.0 ],
		   have_common=true, have_common=false )

# NB. according to:
#   http://sources.redhat.com/ml/bug-automake/2002/msg02066.html
# it's a really bad idea to do conditional tests for things.
#
if test "z$with_win32" = "z"; then
    if test ! $have_common; then
	AC_MSG_ERROR([Library requirements were not met])
    fi

    if test "z$enable_bonobo" = "zyes"; then
	ENABLE_BONOBO="yes"
    else
	enable_bonobo="no"
	ENABLE_BONOBO=""
    fi
    BUILD_WIN32=
else
    if test "z$with_distro" = "z"; then
	with_distro="Win32"
    fi
    if test "z$with_system_gcc" = "z"; then
	with_system_gcc=false;
    fi

    enable_bonobo="no"
    ENABLE_BONOBO=""

    BUILD_WIN32=yes
fi

PKG_CHECK_MODULES( FOO_GNOME, [ gnome-vfs-2.0 \
		   libgnomecups-1.0 >= 0.1.0 ],
		   have_gnome=true, have_gnome=false )

if test "z$with_distro"!="zSuSE" -a ! $have_gnome; then
    AC_MSG_ERROR([Gnome library requirements were not met])
fi

AC_SUBST(ENABLE_BONOBO)
AC_SUBST(BUILD_WIN32)
AM_CONDITIONAL(BONOBO, test "z$ENABLE_BONOBO" = "zyes")

AC_MSG_CHECKING( for tag )
if test "z$with_tag" = "z"; then
   with_tag=`$srcdir/bin/latest-patchset $srcdir/patches $latest_flags`;	
   if test "z$with_tag" = "z"; then
      AC_MSG_ERROR( Can't locate latest patchset )
   fi
fi

if test -f $srcdir/patches/$with_tag/apply; then
   AC_MSG_RESULT( found $with_tag )
else
   AC_MSG_ERROR( Can't locate patch set for: $with_tag )
fi
TAG=$with_tag
AC_SUBST(TAG)

warn_use_download=
AC_MSG_CHECKING( for tag's source )
if test "z$with_src" = "z"; then
   ext_srcdir="$srcdir/src"
   warn_use_download="	./download
"
elif test "${with_src:0:1}" != "/"; then
   AC_MSG_ERROR( --with-src= must be an absolute path );
else
   ext_srcdir=$with_src
   if test -d "$with_src/$TAG"; then
      AC_MSG_RESULT( found '$with_src/$TAG' )
   else
      AC_MSG_WARN([
     Missing pristine unpacked source in $with_src/$TAG
     ]);
   fi
fi

WITH_SRC=$with_src
AC_SUBST(WITH_SRC)

if test "z$with_ooo_builddir" = "z"; then
   if test "z$with_src" = "z"; then
      OOBUILDDIR=$BASEDIR/build/$TAG
   else
      OOBUILDDIR=$WITH_SRC/$TAG
   fi
else
   OOBUILDDIR=$with_ooo_builddir
fi

AC_SUBST(OOBUILDDIR)

LIBART_VERSION=2.3.13
# RH9 (Shrike) and RHEL3 (Taroon) have older libart which we compensate for
if test "z$with_distro" = "zRHTaroon" -o "z$with_distro" = "zRHShrike"; then
   LIBART_VERSION=2.3.11
fi
PKG_CHECK_MODULES( BAA, libart-2.0 >= $LIBART_VERSION,
		   have_baa=true, have_baa=false )

if test "z$with_win32" = "z"; then
    if test ! $have_baa; then
	AC_MSG_ERROR([You need to install libart development headers])
    fi
else
    AC_PATH_PROG(CABEXTRACT, cabextract)
    if test "!" -x "$CABEXTRACT" ; then
      AC_MSG_ERROR([
*** The cabextract utility must be installed.])
    fi
fi

if test "z$with_distro" = "z"; then
   with_distro="Ximian";
fi
DISTRO=$with_distro
AC_SUBST(DISTRO)

VENDORNAME=OpenOffice
if test "z$with_vendor" != "z"; then
   VENDORNAME="$with_vendor"
fi
AC_SUBST(VENDORNAME)

if test "z$VENDORNAME" == "zSuSE" -a "z$with_icons" == "z"; then
   with_icons=KDE
fi

dnl
dnl Debian specific hard-coded defaults.
dnl
if test "z$with_distro" = "Debian"; then
   if test "z$with_system_gcc" = "z"; then
      with_system_gcc=true;
   fi
fi

dnl
dnl Ark Linux specific hard-coded defaults.
dnl
if test "z$with_distro" = "zArk"; then
    if test "z$with_system_gcc" = "z"; then
        with_system_gcc=true
    fi
    if test "z$with_icons" = "z"; then
        with_icons=KDE
    fi
    if test "z$with_vendor" = "z"; then
        with_vendor="Ark Linux"
    fi
fi

JDK_HOME=
if test "z$enable_java" = "zyes"; then
   ENABLE_JAVA=yes
   if test "z$with_jdk_home" != "z"; then
      JDK_HOME=$with_jdk_home
   fi
else
   AC_PATH_PROG(XSLTPROC, xsltproc, no)
   if test "$XSLTPROC" = "no"; then
      AC_MSG_ERROR([xsltproc is required to build without java])
   fi
   ENABLE_JAVA=no
fi
AC_SUBST(ENABLE_JAVA)
AC_SUBST(JDK_HOME)

AC_CHECK_HEADER(security/pam_appl.h, have_pam=true, have_pam=false)
AC_CHECK_HEADER(png.h, have_png=true, have_png=false)
AC_CHECK_HEADER(zlib.h, have_zlib=true, have_zlib=false)

if test "z$with_win32" = "z"; then
    if test ! $have_pam; then
	AC_MSG_ERROR(install pam-devel)
    fi
    if test ! $have_png; then
	AC_MSG_ERROR(install png-devel)
    fi

    if test ! $have_zlib; then
	AC_MSG_ERROR(install zlib-devel)
    fi

    AC_CHECK_PROG(IMG_MAGIC, convert, yes)
    if ! test "z$IMG_MAGIC" = "zyes"; then
       AC_MSG_ERROR( ImageMagick must be installed )
    fi
fi

AC_CHECK_PROG(ACONF, autoconf, yes)
if ! test "z$ACONF" = "zyes"; then
    AC_MSG_ERROR( autoconf must be installed )
fi

AC_CHECK_PROG(FLEX, flex, yes)
if ! test "z$FLEX" = "zyes"; then
   AC_MSG_ERROR( flex must be installed )
fi

AC_CHECK_PROG(BISON, bison, yes)
if ! test "z$BISON" = "zyes"; then
   AC_MSG_ERROR( bison must be installed )
fi

SYSTEM_GCC=true
if test "z$with_system_gcc" = "z"; then
   SYSTEM_GCC=
   with_system_gcc=no
fi
AC_SUBST(SYSTEM_GCC)

BUILD_NCPUS=1
if test "z$with_num_cpus" != "z"; then
   BUILD_NCPUS=$with_num_cpus
fi
AC_SUBST(BUILD_NCPUS)

CCACHE_ALLOWED=""
if test "z$with_ccache_allowed" != "z"; then
   CCACHE_ALLOWED=$with_ccache_allowed
fi
AC_SUBST(CCACHE_ALLOWED)


OOO_ICONS_VER="ooo-icons-OOO_1_1-9"
OOO_ICONS_URI="http://ooo.ximian.com/packages"
if test "$with_icons" = "KDE"; then
    OOO_ICONS_VER="ooo-KDE_icons-OOO_1_1-0.1"
    OOO_ICONS_URI="http://kde.openoffice.org/files/documents/159/1785"
elif test "$with_icons" = "KDE_debug"; then
    OOO_ICONS_VER="ooo-debug_icons-OOO_1_1-0.1"
    OOO_ICONS_URI="http://kde.openoffice.org/files/documents/159/1786"
elif test "$with_icons" = "Bluecurve"; then
    OOO_ICONS_VER="ooo-icons-bluecurve-OOO_1_1-9"
    OOO_ICONS_URI=""
else
    with_icons='default'
fi
AC_SUBST(OOO_ICONS_VER)
AC_SUBST(OOO_ICONS_URI)

AC_MSG_CHECKING([for broken aliases])
if `alias gcc > /dev/null 2>&1`; then
   AC_MSG_ERROR( you can't alias your gcc - that's broken );
fi
AC_MSG_RESULT(ok)

OOO_BINSUFFIX=
OOO_MAJOR=1.1
OOO_VERSION=$OOO_MAJOR.1
AC_SUBST(OOO_VERSION)
AC_SUBST(OOO_BINSUFFIX)

datestamp=`date '+%Y%m%d.%H%M'`;
SNAPSHOT_TAG="$datestamp.$TAG";
AC_SUBST(SNAPSHOT_TAG)

if test "z$enable_bonobo" = "zyes"; then
   AC_CONFIG_SUBDIRS(bonobo)
fi

if test "z$with_installed_ooo_dirname" != "z"; then
   OOOINSTALLDIRNAME=$with_installed_ooo_dirname
else
   OOOINSTALLDIRNAME=ooo-$OOO_MAJOR
fi
AC_SUBST(OOOINSTALLDIRNAME)

AC_CONFIG_FILES([download], [chmod +x download])
AC_CONFIG_FILES([intltool-extract intltool-merge intltool-update], [chmod +x intltool*])
AC_OUTPUT([ 
Makefile
ooo1.1.spec
bin/setup
bin/Makefile
desktop/Makefile
etc/Makefile
fonts/Makefile
patches/Makefile
patches/OOO_1_1/Makefile
patches/OOO_1_1_0/Makefile
patches/OOO_1_1_1/Makefile
po/Makefile
doc/Makefile
src/Makefile
man/Makefile
www/Makefile
www/images/Makefile
www/tinder-scripts/Makefile
])

echo "
Building openoffice
	tag:                $TAG
	src:                $ext_srcdir
	build dir:          $OOBUILDDIR
	distro:             $with_distro
	icons:		    $with_icons
	system gcc:         $with_system_gcc
	ooo-bonobo:         $enable_bonobo
	java:               $ENABLE_JAVA
	ooo-install-dir:    $OOOINSTALLDIRNAME

To build run:
$warn_use_download	make
	sudo make install
"
