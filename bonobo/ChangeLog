2004-03-01  Michael Meeks  <michael@ximian.com>

	* autogen.sh: fix intltool version checks.

2003-10-29  Michael Meeks  <michael@ximian.com>

	* autogen.sh (have_autoconf): don't do an intltoolize,
	we need a very recent, intltool, that is included anyway.

2003-10-10  Martin Kretzschmar  <martink@ximian.com>

	* src/Makefile.am (ooo_bonobo_bin_SOURCES): add services.h,
	string-macros.h

2003-10-08  Martin Kretzschmar  <martink@ximian.com>

	* src/ooo-bonobo-control.cxx (load_uri): make it work repeatedly.

	* src/ooo-bonobo-control.cxx (frame_widget_realize):
	only load on realize if load_uri was called.

2003-10-07  Martin Kretzschmar  <martink@ximian.com>

	* configure.ac: defaults for ooo-builddir.

	* src/ooo-bonobo-control.cxx (zoomable_report_zoom_level_changed):
	implement, use it.

	* data/GNOME_OpenOfficeOrg.server.in.in: add non-standard mime
	types for excel and powerpoint

	* src/ooo-bonobo-control.cxx (zoomable_set_zoom_level_cb)
	(zoom_in_cb, zoom_out_cb, zoom_to_fit_cb, zoom_to_default_cb)
	(ooo_bonobo_control_new): BonoboZoomable implementation (except
	change notification).

	* configure.ac: set LD_LIBRARY_PATH for regmerge and regcomp.

2003-10-06  Martin Kretzschmar  <martink@ximian.com>

	* configure.ac: add default for OOO_INSTALLDIR.

	* src/main.cxx (main): bindtextdomain etc. 

	* src/Makefile.am: #define GNOMELOCALEDIR

	* configure.ac: add de to ALL_LINGUAS.

	* data/Makefile.am: generate, install wrapper script.

	* data/ooo-bonobo.in: wrapper script template.

	* src/Makefile.am: ooo-bonobo->ooo-bonobo.bin, to be called by a
	wrapper script.

	* configure.ac: variable renaming.

	* data/.cvsignore, src/.cvsignore: upd.

2003-10-03  Martin Kretzschmar  <martink@ximian.com>

	* configure.ac: ignore missing headers and programs unless we're
	called with --enable-strict-tests, add --with-ooo-builddir switch.

	* src/remote-uno-helper.cxx (getComponentContext): catch
	InvalidRegistryExcpetion.

	* data/ooo-bonobo-uno-bootstraprc.in: replacement for ~.ini.in

	* data/ooo-bonobo-uno-bootstrap.ini.in: remove
	
	* src/Makefile.am, data/Makefile.am: s/.ini/rc/. There _is_ code
	in OOo that expects rc as extension.

	* data/.cvsignore: ignore ooo-bonobo-uno-bootstraprc

	* src/star-frame-widget.cxx (star_frame_widget_create_view_properties):
	do the right thing for views != writer.

2003-10-03  Michael Meeks  <michael@ximian.com>

	* src/remote-uno-helper.cxx (getComponentContext): impl.

	* src/main.cxx (factory),
	* src/test.cxx (main): remove cut & paste activation code.

	* README: update slightly.

	* src/test.cxx (main): pass the filename in argv.

2003-10-02  Martin Kretzschmar  <martink@ximian.com>

	* src/star-frame-widget.cxx (star_frame_widget_create_view_properties) 
	(star_frame_widget_get_view_properties): new.

	* src/ooo-bonobo-control.cxx (verb_ZoomNormal_cb) 
	(verb_ZoomNormal_cb): implement.

	* src/star-frame-widget.cxx (star_frame_widget_zoom_100) 
	(star_frame_widget_zoom_page_width): implement.

	* src/star-frame-widget.h: add prototypes.

	* src/ooo-bonobo-control.cxx (verb_ZoomFit_cb) 
	(verb_ZoomNormal_cb): implement.

	* data/ooo-bonobo-control-ui.xml: uncomment zoom menu items.
	
	* data/ooo-bonobo-control-ui.xml: add.	

	* src/star-frame-widget.cxx (star_frame_widget_dispatch_slot_url): 
	extract from star_frame_widget_set_fullscreen

	* src/star-frame-widget.h (star_frame_widget_dispatch_slot_url):
	add prototype.

	* src/ooo-bonobo-control.cxx (verb_FileSaveAs_cb) 
	(verb_FileExport_cb, verb_FileExportPDF_cb, verb_FilePrint_cb) 
	(verb_FileProperties_cb, verb_EditCopy_cb) 
	(ooo_bonobo_control_activate): implement some verbs.

	* src/Makefile.am: INCLUDES += -DDATADIR

	* po/POTFILES.in: add data/ooo-bonobo-control-ui.xml.

	* data/Makefile.am (uidir): use -ui.xml files.

2003-10-01  Martin Kretzschmar  <martink@ximian.com>

	* src/star-frame-widget.cxx (star_frame_widget_set_fullscreen):
	extract.
	(star_frame_widget_unrealize): make sure that we don't exit in
	fullscreen mode.

	* src/ooo-bonobo-control.cxx (FrameLoadFileFromUrl): move type
	detection code to this function.

	* src/star-frame-widget.cxx (star_frame_widget_dispose): add.

	* src/remote-uno-helper.cxx (getRemoteComponentContext): start
	ooffice using -norestore.

	* configure.ac: don't require an installed OOo, use libs from the
	solver.

2003-09-30  Martin Kretzschmar  <martink@ximian.com>

	* src/test.cxx (main): 
	* src/ooo-bonobo-control.cxx (frame_widget_realize): 
	* src/star-frame-widget.h: 
	* src/star-frame-widget.cxx (star_frame_widget_create_window_peer) 
	(star_frame_widget_create_frame, star_frame_widget_realize): sanitize.

	* src/ooo-bonobo-control.cxx (load_uri, ooo_bonobo_control_new):
	move more code out of main.cxx.

	* Makefile.am (DISTCLEANFILES): remove the intltool scripts on
	distclean, not on clean.

2003-09-29  Martin Kretzschmar  <martink@ximian.com>

	* src/ooo-bonobo-control.cxx: 
	* src/ooo-bonobo-control.h: new OOoBonoboControl class.

	* src/main.cxx: use OOoBonoboControl.

	* src/star-frame-widget.cxx (star_frame_widget_finalize): add.

	* src/star-frame-widget.h: remove uri from the gobject struct.

	* src/services.h (SERVICENAME_UNOURLRESOLVER): define.

	* src/main.cxx: add typedetection code here too;
	(load_uri): almost as evil as the hardcoded FILENAME.
	(realize): do the FullScreen trick to get rid of the OOo UI.

	* data/GNOME_OpenOfficeOrg.server.in.in: add lots of mime types

	* src/star-frame-widget.cxx, src/star-frame-widget.h
	(star_frame_widget_new): take an XComponentContext as parameter.

	* src/test.cxx, src/main.cxx: upd. for star_frame_widget_new

	* README: upd.

	* src/remote-uno-helper.cxx: 
	* src/remote-uno-helper.h: new, contains getRemoteComponentContext.

	* src/test.cxx: extract getRemoteServiceManager, make it
	getRemoteComponentContext.

	* src/main.cxx: remove the "Button hack," use
	getRemoteComponentContext.

	* src/Makefile.am (test_SOURCES, ooo_bonobo_SOURCES): add
	remote-uno-helper.*

2003-09-26  Martin Kretzschmar  <martink@ximian.com>

	* src/test.cxx (getPipeName, getRemoteServiceManager, main): start
	OOo if it isn't already listening. Similar code can be found in
	the old bonobo-office (and in cuckooo).

	* src/services.h: SERVICENAME_FOO #defines.

	* src/test.cxx:
	* src/star-frame-widget.cxx:
	* src/main.cxx: use SERVICENAME_FOO.

	* src/string-macros.h: add. copy of
	framework/inc/macros/generic.hxx.

	* src/test.cxx, src/main.cxx: use DECLARE_ASCII where possible.
	
	* src/test.cxx (main): add type detection code (removes
	restriction to sxw).

	* src/star-frame-widget.cxx: don't assert x_frame.is().

	* src/main.cxx (FrameLoaderLoadFileFromUrl): 
	* src/test.cxx (FrameLoaderLoadFileFromUrl): use sUrl parameter.

	* configure.ac: use types.rdb, not udkapi.rdb, because we need
	types from offapi.

2003-09-25  Michael Meeks  <michael@ximian.com>

	* src/star-frame-widget.cxx
	(star_frame_widget_new): pass the service factory in.

2003-09-24  Martin Kretzschmar  <martink@ximian.com>

	* initial CVS import.

