ooo-build-1.1.55

    + bug fixes
	+ CONFIGURE_OPTIONS from environment (Arkadiusz)
	+ de-bashizing (Arkadiusz)
	+ misc build fixes (Arkadiusz, Chris, Rene, Michael)
    + features
	+ VBA (Michael)
	    + attribute parsing
	    + improved tests
	    + more VBA than StarBasic compatible
	+ KDE (Kendy)
	    + helper program integration
	    + splash / branding
	+ Novell (Internal)
	    + file-a-bug button
	+ PLD Linux support (Arkadiusz Miskiewicz)
	    + glibc compile fix
	    + splash/branding
	    + stlport 4.6.x compile
	+ Ark Linux patches (Bernhard Rosenkraenzer)
	    [ still underway ]
	    + branding

ooo-build-1.1.54

    + bug fixes
	+ afm metric load fix (Chris)
	+ psprint cups fix (Chris)
	+ read VBA only once (Michael)
	+ allow upgrade from 1.1.0 (Michael)
	    + don't follow 'work' symlink on upgrade
	+ linkoo relative path check (Michael, chriss)
	+ fix norwegian translation reverse patching issue (Michael)
	+ Win32 (Michael)
	    + fix 'relocate'
	    + don't try to symlink
	    + set executable flag on copied dlls
	    + extend disable-java coverage
	    + add oowintool to help autoconfigure
	+ ca translation updates (Jordi Mallach)
	+ system/db misc. bits (Rene)
    + features
	+ ooo-build specific versioning (Michael)
	+ setup license agreement less painful (Michael)
	+ use gnome-open for default handlers on RH (Dan)
	+ add expanded VBA macro test-sheet.
    + big SuSE patch merge (pmladek, paveljanik, kendy)
	+ SuSE distro / vendor support
	+ SuSE splash/about
	+ improve generic print resolution
	+ use ARCH_FLAGS
	+ fix translation/lang-map problems
	+ improved font mappings
	+ use system libstdc++
	+ shrink exported html in some 8bit locales
	+ translate some calc strings.
	+ enlarged xlate-lang set
    + pending
	+ evolution-data-server integration (Amit)
	+ spellcheck speedup (Dan)

ooo-build-1.1.53

    + bug fixes
	+ internal zlib fix (Michael)
	+ build fixage / cleanups (Chris)
	+ word-count localisation (Dan)
	+ neon security fix (Dan)
	+ upgrade/path-re-write on relocate (Michael, Dan)
    + features
	+ system-python usage (Martin)
	+ Debian .fi translation tweak (Rene)
	+ --with-icons=KDE icon set (Jan)
	+ build conditionals cleanup (Dan, Rene)
	+ better deliver-compare speedup (Michael)
	+ Win32 build (Michael, Vivek, Amit)
	    + MSVS.Net 2003 build fixes
	    + berkeleydb build dmake'd
	    + improved 'download' support
	    + internal libart support
	    + auto-unpack unicows/dbghelp
	    + misc. source fixes for VC++ ( & Krishnan )
	    + initial compiler auto-detection improvements
	    + accelerated icon rendering
	    + disable-java works on Win32
    + pending / experimental
	+ native widget support backport (Jan) 

ooo-build-1.1.52

    + bug fixes
	+ calm beep mania on certain frame-sets (Michael)
	+ crash-bindings re-backport fix (Michael)
	+ fix excel debugging for Unix (Michael)
	+ recent-files FMR (Julian Seward)
	+ BUILD_SPECIAL workarounds (Michael)
	+ build less of icu (Rene)
	+ build fixes (Dan, Michael, Vivek)
	+ Escher / PPT
	    + deep nesting crasher bug (Michael)
	    + hang on exported PPT (Sven Jacobi)
	+ wrapper upgrade cleanly (Michael, Chris)

    + features
	+ target 1.1.1 (Rene, Chris, Michael)
	+ --with-ccache-allowed (Jan Holesovsky)
	+ save-as right-click on images (Martin)
	+ use system packages (Rene)
	    + neon
	    + Xrender
	    + ODBC
	+ macros - Ximian-only [testing] (Michael)
	    + import and execute large >64k macros
	    + un-comment / execute macros by default 
	    + import / export Excel macro references
	+ XL macro reference round-trip (Michael)
	+ norwegian translation framework (Rene & Gaute)

    + pending / in-progress
	+ VBA macro export (Michael)
	+ initial Win32 support (Vivek, Michael)
	    + package scaled icons separately
	    + get tools to handle \r\n (Ravi Kiran)
	+ back-ported CUPS support (Chris)

ooo-build-1.1.51

    + bug fixes
	+ calc (Michael)
	    + complex number fixes 
	    + multinomial fix
	    + excel I/O error mapping
	    + singleton / implicit intersection fix
	    + cursor movement fix (Amit)
	+ font speedup / fixes (Michael)
	+ gtk theming color issues (Dan)
	+ default font / fontconfig sync fix (Dan)
	+ recent files
	    + NFS / flock lockup (Dan)
	    + missing .recently-used case (Anil)
	+ flash compile breakage (Michael, Rafel Roszak)
	+ misc. build fixes (Chris, Rene)
    + features
	+ 1.1.1 support (Rene)
	    + large number of updated patches
	    + build more langs: hebrew, hindi, thai
	+ --enable-symbols switch (Dan)
	+ new name-account mappings (Utomo, Michael)
	+ improved parallel build support (Dan)
	+ debug helper (Michael)
	+ updated user faq (Carlos Alberto)
	+ more / improved icons (Jimmac)


ooo-build-1.1.50

    + bug fixes
	+ recent-files problems / re-hash (Anil, Rene)
	+ gtk+ theme color sync (Dan)
	+ identify 111a version (Rene)
	+ explicit LGPL/SISSL text (Michael)
	+ check for zlib.h (Michael)
	+ re-enable Java workarounds (Michael)
	+ system curl problem (Rene)
	+ GetCorrectPath [really] (Michael, PavelJanik)
	+ distribute the right patches (Martin)
    + features
	+ patch only with terminals (Michael)
	+ share /usr/lib/ooo-1.1 on RH (Dan)
	+ full 1.1.1a patch-set (Martin, Rene)

ooo-build-1.1.49

    + bug fixes
    	+ recent-files portability/compile (Anil)
	+ set_soenv.in: GetCorrectPath (Michael)
	+ system spell problem (Rene)
    + features
	+ quilt output from 'apply' (Martin)
	+ more icons mapped (Michael)
	+ partial 1.1.1a support (Martin)

ooo-build-1.1.48

    + features
	+ MS OLE <-> OO.o; big formula improvement (Michael)
        + allow --with-jdk-home (Dan)
        + ergonomic rename update (Michael)
        + recent-files dropped glib dep (Anil Bahtia)
	+ tiny pagein speedup (Michael)
	+ more hacker documentation (Michael)
	+ RH specific
	    + vendor splash (Dan)
	+ Debian specific (Rene)
	    + sensible browser
	    + xinerama portability bits
	+ use system bits (Rene)
	    + NAS
            + use system DB [4.0, 4.1 etc.]
	    + sane
	    + curl
	    + myspell
	+ use mozilla not netscape (Rene)
	+ use ~/Documents only if exists (Chris, Mdk)
        + OOO_RH_DEBUG env. var for RH debugging (Dan)
	+ calc, show row sensitisation (Amit)
    + bug fixes
        + desktop file generation (Michael)
        + build with freetype 2.1.7 (Rene)
        + Linux/Sparc no-java builds (Chris)
        + hyperlink toolbar alpha (Michael)
        + bin ant check for no-java (Michael)
	+ wizard/misc. build fixes for Java (Dan)
	+ html export marking spelling errors (Martin)
	+ don't source || exit 1; for RH 7.3 (Michael)
	+ bindings crash fix update (Michael)
	+ really distribute ld speedup patch (Michael)
	+ X fonts if no fontconfig (Chris)
	+ symbols/crash fix (Chris)
	+ install-dict no-Twig, cleanups (Michael)
	+ $(CC) for sparc assember (Chris)
	+ stlport include (Rene)

ooo-build-1.1.47

	+ bug fixes
		+ font mapping reversion (Michael)
		+ valgrind clean back-port (Michael)
		+ menu check-item fixes (Amit Shrivastava)
		+ setup -nogui crasher fix (Dan)
		+ RH - lang pack fixage (Dan)
		+ KDE UI / scaling fixes (Chris)
		+ build fixes (Dan)
		+ desktop muddle up fix (Chris)
		+ more build / vendor conditionals (Dan)

ooo-build-1.1.46

	+ features
		+ UI fixes
			+ much improved OpenSymbol font (Jimmac)
			+ improved font mapping (Dan, Michael)
			+ more style navigator icons (Tigert)
			+ nicer looking ruler scaling (Michael)
			+ don't grey bullets as fields (Michael)
			+ prune some ugly symbols (Anil Bhatia)
		+ System bits
			+ 3 new targets: RHFedora, RHTaroon, RHShrike (Dan)
			+ --with-installed-ooo-dirname (Dan)
			+ system libdb integration (Dan)
			+ faster help linkage (Dan)
			+ OOO_FORCE_DESKTOP flag (Michael, Philipp L, Chris)
			+ use OOO_MS_DEFAULTS env. var (Michael)
			+ use fork for 2.6 kernels (Ken Foskey, Rene)
		+ Red Hat specific patches (Dan, et. al)
			+ nautilus/VFS integration bits
			+ system neon fix
			+ libart cut/paste for old systems
			+ system freetype re-hash
	+ bug fixes
		+ critical
			+ parallel / build script fixes (Dan)
			+ svx lockup fix (Dan)
			+ impress I/O error fix (Martin, Christian L)
			+ help / language fallback issue (Chris)
		+ misc
			+ re-root - only scan CVS dirs (Michael)
			+ missing MySQL data source fix (Rene)
			+ uno asm fixes (Dan)
			+ svtools build deps fix (Michael)
			+ improved default font size (Chris)


ooo-build-1.1.45

	+ features
		+ build without Java (Josh Triplett)
		+ tweaks
			+ improved rename ergonomics (Jayant Madavi)
			+ set TYPE_UTILITY on utility windows (Michael)
			+ KDE font size/integration improvements (Chris)
			+ larger default font in non-Gnome/KDE WM (Chris)
			+ optional dmake build (Rene)
			+ conditionalized fontconfig use (Rene)
			+ i18n honours LC_CTYPE (Michael)
			+ alpha compositing speedups (Michael)
			+ disable style-list popup on 1st run (Michael)
			+ improved icon scaling helper (Martin K)
		+ build infrastructure
			+ improved patch descriptons (Michael, Martin)
			+ simple CVS re-rooting tool (Michael)
			+ deliver hard links to save space (Chris, Michael)
	+ bug fixes
		+ MS bullet export problems (Michael, Will Lachance)
		+ fix running setup within a mount --bind tree (Chris)
		+ evolution addressbook i18n fix (Martin K)


ooo-build-1.1.44

	+ features
		+ new, sized, alpha fixed icons (Tigert, Jimmac)
		+ scroll-wheel quickhelp less annoying (Michael)
		+ system-zlib, system-getopt (from Mdk, Rene)
		+ nice bonobo component (Martin Kretzchmar)
			+ configure with --enable-bonobo
	+ bug fixes
		+ relocate now prunes stale .dpcc files (Michael)
		+ problems in toolbox customize (Michael)
		+ --with-system-gcc fix (Michael, Robin Cook)
		+ print-scale fix (Dan Williams, Michael)
		+ File->New i18n fix (Michael)

ooo-build-1.1.43

	+ features
		+ more database etc. icons (Tigert, Jimmac)
		+ drop the 1.1 bin-suffix
		+ 'word count' translations (Martin, Gwenole B)
		+ debian-specific splash (Rene)
		+ binutils patch to speedup ld (Dirk Mueller, Michael)
	+ bug fixes
		+ lots of alpha problems fixed (Michael)
		+ initialize some values (Julian Seward)
		+ File->New/Autopilot menu i18n (Martin)
		+ sparc linux build fixes (Chris)
		+ bind mount setup fix (Chris)
		+ use unzip not jar for building (Rene)
		+ linkoo handles iso.res vs ooo.res (Michael)
		+ fontconfig crasher bug (Michael)
		+ fixes to mozilla disabling bits (Martin)
		+ clobber irritating raise-on-load bits (Michael)
	+ Debian specific
		+ disable odk build [ non free stuff ] (Rene)
		+ fontconfigize padmin (Chris)
		+ use more Ximian patches in Debian builds (Chris)

ooo-build-1.1.42

	+ features
		+ pull up to OO.o 1.1.0 final
		+ loads more nice icons (Tigert, Jimmac)
		+ slower & cleaner alpha rendering (Michael)
		+ nicer toolbox item prelight (Michael)
	+ bug fixes (Michael)
		+ PDF export / font-subsetting
		+ setup LD_LIBRARY_PATH problems (Anders)
		+ 2nd time oddness with -writer flag
		+ server render splash pixmap
		+ use the right branded icon set

ooo-build-1.1.40

	+ features
		+ switch to the RC4 tree (Michael)
		+ more icons / better coverage (Jimmac)
	+ bug fixes
		+ LD_LIBRARY_PATH installer issue (Anders)
		+ correct palette pop-down (Michael, Philipp L)
		+ sensible coluor palette sizing (Michael)
		+ system font sync fix (Michael)
		+ updated magicpoint users help (Michael)
		+ web fixes (Julian Seward)
	+ translations (last few releases)
		+ da (Kenneth Christiansen)
		+ sr (Danilo Segan)
		+ fr (Christophe Merlet)
		+ ca (Jordi Mallach)
		+ vi,wa (Pablo Saratxaga)
		+ pl (Artur Flinta)
		+ nl (Vincent van Adrighem)
		+ cs (Miloslav Trmac)
		+ pt (Duarte Loreto)

ooo-build-1.1.39

	+ features
		+ switch to the RC3 tree (Michael)
	+ bug fixes
		+ font munging fixes (Michael)
		+ wrapper cleans (Michael)
		+ improved patching fuzziness (Michael)
		+ more word count translations (Martin)
		+ crashdump build fixes (Michael)
		+ bin MS format default duplication (Michael)

ooo-build-1.1.38
	+ bug fixes
		+ vicious bindings crasher (Michael)
		+ export 'no color' bg to .doc (Chris)
		+ source EOF massage (Chris)
		+ build fixes (Michael, Martin)
	+ features
		+ elisp for OO.o source (Martin)
	+ pending
		+ improved gtk+ integration
		+ layout prototype

ooo-build-1.1.37
	+ bug fixes
		+ metric font mappings for XD2 fonts (Michael)
		+ ccache fixes (Anders Carlsson)
		+ word count fixes (Martin Kretzschmar)
		+ crashdump strangeness (Michael)
		+ misc. build fixes (Michael, Chris, Martin)
	+ features
		+ save 1.5Gb in lang-pack (Chris, Michael)
		+ toolbox (Martin)
			+ pretty zoom combo
			+ death of URL combo
		+ translated .desktop files (Carlos Perelló Marín)
			+ de (Christian neumair)
			+ da (Ole Laursen)
			+ zh (Abel Cheung)
			+ de (Martin)
			+ no (Kjartan Maraas)
			+ sv (Christian Rose)
			+ sq (Laurent Dhima)
			+ es (Carlos)
		+ Man pages (Rene Engelhard)
		+ beep far less (Michael)
		+ build bits
			+ conditionals/paths (Michael)
			+ much improved patching (Chris)
			+ --with-system-gcc (Michael)
			+ disable broken moz bits (Michael)
			+ linkoo improvements (Martin)
			+ Gnome/gtk+ build fixage (Rene)
			+ build less of gcc (Nick Hudson)
		+ Debian patches merged (Chris, Rene, Martin)
			+ config switch expansion, bin lzwc
			+ drop rpaths, sal_debug, stlport dynlink
			+ html fix, system python/aclocal
		+ shrink perl wrapper (Michael)
			+ obey LANG env.
			+ obey 'COMPATIBLE' for MS export.
		+ html export (Michael)
			+ nicer icons (Jimmac)
			+ export .png not .gif
	+ pending (Michael)
		+ glib/gtk+ loop integration
		+ some layout bits.

ooo-build-1.1.35
	+ bug fixes
		+ fix wrapper symlinkage / Makefile cruft.
		+ default prefix to /usr
	+ featurelets
		+ oolink support for connectivity libraries

ooo-build-1.1.34
	+ bug fixes
		+ fix configuration patches (system fonts etc.)
		+ icon composite fixes
		+ register Gnome VFS UCP properly
		+ linkoo checks for abs. path (Martin Kretzschmar)
		+ bin bogus .desktop user install
		+ fix ~/Documents as default save loc.
		+ print/error dialog fix
		+ delete/backspace switch in calc.

	+ features
		+ Tools->Word Count in menus
		+ cleanup unzip code (Martin, Michael)
		+ add web/math/template icons (Martin / Jimmac)
		+ build / user guide updates
		+ more build cleans
		+ menu / check rendering forward-port

ooo-build-1.1.33
	+ scale czech,portbr,slovak icons (Martin Kretzschmar)
	+ switch to RC3_030729 snapshot.
	+ add configure --enable-devel to specify target
	+ misc. build fixes / cleans

ooo-build-1.1.31
	+ add --with-src for package builds

ooo-build-1.1.30
	+ import into gnome CVS.
	+ re-hash build / re-arrange
