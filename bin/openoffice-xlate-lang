#!/usr/bin/perl

use strict;

my $progname=$0; $progname = $& if $progname =~ m,[^/]+$,;

my %PREFIX;   # used to search for prefix numbers
my %ISOCODE;  # used to search for iso codes
my %LANGUAGE; # used to search for language names

#=======================================================================
# initialisation code - stuff the DATA into the CODES hash
#=======================================================================
sub init {

    my $prefix;
    my $code;
    my $name;


    while (<DATA>)
    {
        next unless /\S/;
        chop;
        ($prefix, $code, $name ) = split(/:/, $_, 3);
        $PREFIX{$prefix} = $prefix;
        $PREFIX{$code} = $prefix;
        $PREFIX{$name} = $prefix;

        $ISOCODE{$prefix} = $code;
        $ISOCODE{$code} = $code;
        $ISOCODE{$name} = $code;
	
        $LANGUAGE{$prefix} = $name;
        $LANGUAGE{$code} = $name;
        $LANGUAGE{$name} = $name;
    }
}


#=======================================================================
# usage - error message
#=======================================================================
sub usage {
    my $errmsg = shift;
    my $errcode = shift;
    print STDERR "$progname: $errmsg\n" if $errmsg;
    print STDERR "$progname: Converts between prefix codes, iso codes and langnames\n";
    print STDERR " Usage: $progname (-i|-l|-p|-h) <code>|all\n";
    print STDERR "  -i <code>: convert prefix to iso code (ex: 03 -> pt)\n";
    print STDERR "  -l <code>: convert iso code to language name (ex: pt -> portuguese)\n";
    print STDERR "  -p <code>: convert iso code to prefix (ex: pt -> 03)\n";
    print STDERR "  the code can either be an iso code, a prefix or even a language name\n";
    print STDERR "  The special code \"all\" asks for all possible values.\n\n";
    print STDERR "  -h : print this help\n";
    exit $errcode;
}

#=======================================================================
# main - 
#=======================================================================
init();

my ($LanguageCode, $LanguageMap);

while ($ARGV[0] =~ /^-/) {
    $_ = shift;
    if (m/^-i/) {
	$LanguageMap = \%ISOCODE;
    }
    elsif (m/^-l/) {
	$LanguageMap = \%LANGUAGE;
    }
    elsif (m/^-p/) {
	$LanguageMap = \%PREFIX;
    }
    elsif (m/^-h/) {
	usage("",0);
    }
    else {
	usage ("unknown option $_",1);
    }
}

usage ("no operation specified on command line",1)
    if (!$LanguageMap);

usage ("no language code specified on command line",1)
    if (!($LanguageCode = shift));

if ($LanguageCode =~ (m/^all$/)) {
    # Asked for all codes
    my $old="";
    foreach my $key (sort values %$LanguageMap) {
	if ($key ne $old) {
	    print "$key ";
	    $old=$key;
	}
    }
    print "\n";
    exit 0;
}

usage ("no mapping found for $LanguageCode\n",1)
	if (!(%$LanguageMap->{$LanguageCode}));

print $LanguageMap->{$LanguageCode}, "\n";

1;

__DATA__
01:en:english_us
03:pt:portuguese
07:ru:russian
30:el:greek
31:nl:dutch
33:fr:french
34:es:spanish
35:fi:finnish
36:hu:hungarian
37:ca:catalan
39:it:italian
42:cs:czech
43:sk:slovak
45:da:danish
46:sv:swedish
48:pl:polish
49:de:german
50:sl:slovenian
77:et:estonian
81:ja:japanese
82:ko:korean
86:zh-CN:chinese_simplified
88:zh-TW:chinese_traditional
90:tr:turkish
96:ar:arabic
