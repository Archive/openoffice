
	We do quick-start in ooo-wrapper.in by handling the extra
--session-quickstart argument.

	However; the OO.o internal quickstart code is a mess:

* From the desktop end

	* framework/source/services/desktop.cxx: it seems that
xQuickLauncher isn't => always quitting. Added via
'addTerminateListener'; two listeners:

** QuickLauncher: 'desktop.QuickstartWrapper'.

	The shutdown icon sits in the systray and allows the user to
keep the office instance running for quicker restart. Implemented in:
sfx2/source/appl/shutdownicon.cxx - but that has to be a throw-back,
and doesn't appear to work.


** PipeTerminator: 'OfficeIPCThreadController'

	* desktop/source/app/officeipcthread.cxx: here is where the
action lurks; seems to have no discrimination between a session
shutdown vs. a user terminate. cf.


* Session management:

	* vcl/unx/source/app/sm.cxx (ShutDownHdl): calls 'ShutDown' on
GetSalData()->pFirstFrame_ - if we have one.


* From the 'quickstart' angle

	+ -headless + -invisible seems to work modulo 'exit', we could
do with a hacked version of the 'shutdown icon' thing that works with
session management.

	+ ARGH - so we have to hack the OfficeIPCFooBaa vetoing baz,
we will provide a few really nasty hacks - to make life worthwhile. We
need to connect (somehow) to the SM death signal, and in that case emit
something nasty like shutdownicon.cxx (terminateDesktop).

	+ Ugly ongoing bug - multi-display stuff is uber-broken.

	+ Can we create an 'immortal' first toplevel frame, that
responds to 'Shutdown' from the SM; and disobey's Exit ?

'Exit' ->
	sfx2/source/appl/appserv.cxx: xDesktop->terminate.

'Sm Quit' ->
	SalFrameData::Shutdown ->
		{ return pProc_( pInst_, pFrame_, SALEVENT_SHUTDOWN, 0 ); }

	SALEVENT_CLOSE vs. SALEVENT_SHUTDOWN ...
	-> vcl/source/window/winproc.cxx (ImplWindowFrameProc)
		-> GetpApp->QueryExit -> 
			-> Does a 'Close' on the app
		-> Application::Quit
			Application::PostUserEvent(
				STATIC_LINK( NULL, ImplSVAppData, ImplQuitMsg ) );
			sets:
				ImplGetSVData()->maAppData.mbAppQuit = TRUE;
			-> svapp.cxx: Application::Execute terminates





