Accessibility: towards a Free software (Java-free) future.

    The crucial a11y code in OO.o will not be adopted due to the
painful Java requirement, these are some notes on potentially
circumventing this, and bridging directly to at-spi.

* code

accessibility/bride/org/openoffice/
    accessibility/*
	Windows related accessibility code ?
    java/accessibility/*
	Java mapping from UNO <-> Java bindings.


accessibility/

offapi/com/sun/star/accessibility/
	XAccessibleContent.idl: equivalent to Accessible.idl
	XAccessibleComponent.idl: AccessibleComponent.idl
etc.
	What is required is simply a _lot_ of typing / type-mapping.


Hooks into the toolkit done with eg.:
	unoToolkit.addKeyHandler(new KeyHandler());
