* Groupwise / ODMA work:

** Final status:

    + Version mismatch causes grief
    + OO.o UCP API usage very odd, pwrt 'transfer' (Save)
	+ always uses SaveAs (with NULL name) [ bFolder brokenness ? ]
	+ does OpenDoc / re-save having done the transfer [ most odd ]
    + Not convinced that we can hook 'Close' properly
      without more core changes

** Hidden gotchas

    + Adding a key of name length > 8 chars to ODMA32 registry
      entry results in silent fatal errors opening ODMA connection.

** Version 1.0: (Groupwise):

ODMRegisterApp
ODMUnRegisterApp
ODMSelectDoc
ODMOpenDoc
ODMSaveDoc
ODMCloseDoc
ODMNewDoc
ODMSaveAs
ODMActivate
ODMGetDocInfo
ODMSetDocInfo
ODMGetDMSInfo
ODMGetODMInterface
ODMGetLeadMoniker
ODMQueryInterface

** OO.o tests (for version 2.0):

ODMRegisterApp
ODMUnRegisterApp
ODMSelectDoc
ODMOpenDoc
ODMSaveDoc
ODMCloseDoc
ODMNewDoc
ODMSaveAs
ODMActivate
ODMGetDocInfo
ODMSetDocInfo
ODMGetDMSInfo
ODMGetDMSCount
ODMGetDMSList
ODMGetDMS
ODMSetDMS
ODMQueryExecute
ODMQueryGetResults
ODMQueryClose
ODMCloseDocEx
ODMSaveAsEx
ODMSaveDocEx
ODMSelectDocEx
ODMQueryCapability
ODMSetDocEvent
ODMGetAlternateContent
ODMSetAlternateContent
ODMGetDocRelation
ODMSetDocRelation

** Differences

* 'OpenFolder' / folder browse functionality:
    < missing ODMQueryExecute >	    - used in queryContentProperty
    < missing ODMQueryGetResults >  - used in getResult
    < missing ODMQueryClose >	    - used in getResult

* Extended versions of things we need:
	[ for use of the ODM_SILENT flag ]
    < missing ODMCloseDocEx >	- used in closeDocument
    < missing ODMSaveDocEx >	- used for saveDocument
    < missing ODMSaveAsEx >	- used for 'transfer' (save) command

< missing ODMGetDMS >		- unused
< missing ODMGetDMSCount >	- unused
< missing ODMGetDMSList >	- unused
< missing ODMSetDMS >		- unused
< missing ODMQuerySelectDocEx >	- unused
< missing ODMQueryCapability >	- unused
< missing ODMSetDocEvent >	- unused
< missing ODMGetAlternateContent >  - unused
< missing ODMSetAlternateContent >  - unused
< missing ODMSetDocRelation >	    - unused
< missing ODMGetDocRelation >	    - unused

[ unused but in GW version ]:
ODMGetODMInterface
ODMGetLeadMoniker
ODMQueryInterface


